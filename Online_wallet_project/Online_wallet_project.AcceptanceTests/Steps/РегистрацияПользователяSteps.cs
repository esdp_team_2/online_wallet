﻿using NUnit.Framework;
using Online_wallet_project.AcceptanceTests.Extensions;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class РегистрацияПользователяSteps: CommonSteps
    {
        [AfterScenario()]
        private void Down()
        {
            WebDriverManager.TreadDown();
        }

        [Given(@"я нахожусь на странице регистрации пользователя ""(.*)""")]
        public void ДопустимЯНахожусьНаСтраницеРегистрацииПользователя(string site)
        {
            base.OpenPage(site);
        }

        [Then(@"вижу форму регистрации")]
        public void ТоВижуФормуРегистрации()
        {
            Assert.IsTrue(base.ElementContainsText("css", "h2", "Регистрация пользователя"));
        }

        [Then(@"я ввожу данные в поля логин ""(.*)""  имя ""(.*)"" фамилия ""(.*)"" телефон ""(.*)"" почта ""(.*)"" пароль и подтверждения пароля ""(.*)""")]
        public void ТоЯВвожуДанныеВПоляЛогинИмяФамилияТелефонПочтаПарольИПодтвержденияПароля(string login, string firstname, string lastName, int phone, string email, string password)
        {
            base.ElementContainsIdinjectText("UserName",login);
            base.ElementContainsIdinjectText("FirstName", firstname);
            base.ElementContainsIdinjectText("LastName",lastName);
            base.ElementContainsIdinjectText("PhoneNumber", phone.ToString());
            base.ElementContainsIdinjectText("Email",email);
            base.ElementContainsIdinjectText("Password", password);
            base.ElementContainsIdinjectText("ConfirmPassword",password);
        }

        [When(@"я нажимаю кнопку регистрировать")]
        public void ЕслиЯНажимаюКнопкуРегистрировать()
        {
            base.ElementContainsClick("class", "btn");
        }

        [Then(@"перехожу на страницу пользователя")]
        public void ТоПерехожуНаСтраницуПользователя()
        {
            Assert.IsTrue(base.ElementContainsText("css", "h2", "Профиль пользователя"));
        }
    }
}
