﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Online_wallet_project.AcceptanceTests.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class CommonSteps
    {
        private IWebDriver driver = WebDriverManager.GetDriver();

        public void OpenPage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public bool ElementContainsText(string typeSelector, string nameSelector, string text)
        {
            bool result = false;
            switch (typeSelector)
            {
                case "css":
                    IWebElement searchSelectorCss = driver.FindElement(By.CssSelector(nameSelector));
                    if (searchSelectorCss.Text.Contains(text))
                    {
                        result = true;
                    }
                    break;
                case "id":
                    IWebElement searchButtonId = driver.FindElement(By.Id(nameSelector));
                    if (searchButtonId.Text.Contains(text))
                    {
                        result = true;
                    }
                    break;
                case "class":
                    IWebElement searchButtonClass = driver.FindElement(By.ClassName(nameSelector));
                    if (searchButtonClass.Text.Contains(text))
                    {
                        result = true;
                    }
                    break;
            }

            return result;
        }

        public void ElementContainsIdinjectText(string id, string text)
        {
            IWebElement searchElement = driver.FindElement(By.Id(id));
            searchElement.SendKeys(text);
        }

        public void ElementContainsClick(string typeSearch, string name)
        {
            switch (typeSearch)
            {
                case "css":
                    IWebElement searchButtonCss = driver.FindElement(By.CssSelector(name));
                    searchButtonCss.Click();
                    break;
                case "id":
                    IWebElement searchButtonId = driver.FindElement(By.Id(name));
                    searchButtonId.Click();
                    break;
                case "class":
                    IWebElement searchButtonClass = driver.FindElement(By.ClassName(name));
                    searchButtonClass.Click();
                    break;
            }
        }
    }
}
