﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_wallet_project.AcceptanceTests.Models;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("OnlineWalletDatabase")
        {
        }

        public DbSet<BlackPassword> BlackPasswords { get; set; }
        public DbSet<AspNetUsers> Users { get; set; }
    }
}
