﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class АвторизацияАдминистратораSteps: CommonSteps
    {
        [Given(@"я открываю страницу авторизации администратора нашего проекта ""(.*)""")]
        public void ДопустимЯОткрываюСтраницуАвторизацииАдминистратораНашегоПроекта(string site)
        {
            base.OpenPage(site);
        }
        
        [Given(@"я ввожу логин ""(.*)"" и пароль ""(.*)""")]
        public void ДопустимЯВвожуЛогинИПароль(string login, string password)
        {
            if (!FindUser(login, password))
            {
                if (login != "Admin")
                {
                    AddUser(login,password);
                }
            }
            base.ElementContainsIdinjectText("Login",login);
            base.ElementContainsIdinjectText("Password",password);
        }
        
        [When(@"я нажимаю на кнопку ""(.*)""")]
        public void ЕслиЯНажимаюНаКнопку(string namebutton)
        {
            base.ElementContainsClick("class","btn");
        }
        
        [Then(@"поподаю на страницу администратора")]
        public void ТоПоподаюНаСтраницуАдминистратора()
        {
            Assert.IsTrue(base.ElementContainsText("css","h2", "Панель Администратора"));
        }
    }
}

