﻿using System;
using NUnit.Framework;
using Online_wallet_project.AcceptanceTests.Extensions;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class АвторизацияПользователяSteps:CommonSteps
    {
        [AfterScenario()]
        public void Down()
        {
            WebDriverManager.TreadDown();
        }

        [Then(@"я вижу профиль пользователя")]
        public void ТоЯВижуПрофильПользователя()
        {
            Assert.IsTrue(base.ElementContainsText("css", "h2", "Профиль пользователя"));
        }
    }
}
