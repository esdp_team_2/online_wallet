﻿using System;
using System.Threading;
using NUnit.Framework;
using Online_wallet_project.AcceptanceTests.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class ЯХочуЗайтиНаСайтНашегоПроектаSteps : CommonSteps
    {
        [AfterScenario()]
        private void Down()
        {
            WebDriverManager.TreadDown();
        }

        [Given(@"я открываю сайт нашего проекта ""(.*)""")]
        public void ДопустимЯОткрываюСайтНашегоПроекта(string site)
        {
            base.OpenPage(site);
        }
        
        [Then(@"вижу в результате приветствие")]
        public void ТоВижуВРезультатеПриветствие()
        {
            Assert.IsTrue(base.ElementContainsText("css", "h2", "Вход в систему"));
        }
    }
}
