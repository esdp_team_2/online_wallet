﻿using System;
using NUnit.Framework;
using Online_wallet_project.AcceptanceTests.Extensions;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class ПроверкаНаНеправильнуюПопыткуАвторизацииSteps: CommonSteps
    {
        private void Down()
        {
            WebDriverManager.TreadDown();
        }

        [Then(@"вижу ошибку об не правильно введенных данных")]
        public void ТоВижуОшибкуОбНеПравильноВведенныхДанных()
        {
            Assert.IsTrue(base.ElementContainsText("class", "alert-dismissible", "×\r\nНеправильный логин и/или пароль"));
        }
    }
}
