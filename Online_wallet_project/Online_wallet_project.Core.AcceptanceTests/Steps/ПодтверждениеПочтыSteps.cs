﻿using System;
using System.Threading;
using NUnit.Framework;
using Online_wallet_project.AcceptanceTests.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    [Binding]
    public class ПодтверждениеПочтыSteps:CommonSteps
    {
        [Given(@"я нахожусь на сайте почты ""(.*)""")]
        public void ДопустимЯНахожусьНаСайтеПочты(string site)
        {
            base.OpenPage(site);
        }

        [Given(@"я ввожу от почты логин ""(.*)"" и пароль ""(.*)""")]
        public void ДопустимЯВвожуОтПочтыЛогинИПароль(string login, string password)
        {
            IWebElement searchLogin = WebDriverManager.GetDriver().FindElement(By.Id("mailbox:login"));
            searchLogin.SendKeys(login);
            IWebElement searchPassword = WebDriverManager.GetDriver().FindElement(By.Id("mailbox:password"));
            searchPassword.SendKeys(password);
            searchPassword.Submit();
        }
        
        [Then(@"я перехожу во входящии письма")]
        public void ТоЯПерехожуВоВходящииПисьма()
        {
            Thread.Sleep(5000);

            IWebElement searchInboxMail = WebDriverManager.GetDriver().FindElement(By.ClassName("b-nav__item__text"));
            Assert.IsTrue(searchInboxMail.Text.Contains("Входящие"));
        }

        [Then(@"вижу свое письмо с названием")]
        public void ТоВижуСвоеПисьмоСНазванием()
        {
            IWebElement searchMail = WebDriverManager.GetDriver().FindElement(By.ClassName("b-datalist__item__addr"));
            Assert.IsTrue(searchMail.Text.Contains("Администрация сайта"));
        }

        [When(@"я его открываю")]
        public void ЕслиЯЕгоОткрываю()
        {
            IWebElement searchMail = WebDriverManager.GetDriver().FindElement(By.ClassName("b-datalist__item__addr"));
            Assert.IsTrue(searchMail.Text.Contains("Администрация сайта"));
            searchMail.Click();
        }

        [Then(@"вижу ссылку на подтверждение")]
        public void ТоВижуСсылкуНаПодтверждение()
        {
            Thread.Sleep(2000);
            IWebElement searchLink = WebDriverManager.GetDriver().FindElement(By.Id("z_mailru_css_attribute_postfix"));
            Assert.IsTrue(searchLink.Text.Contains("cсылка для перехода"));
        }

        [When(@"я нажимаю нажимаю ее")]
        public void ЕслиЯНажимаюНажимаюЕе()
        {
            IWebElement searchLink = WebDriverManager.GetDriver().FindElement(By.Id("z_mailru_css_attribute_postfix"));
            if (searchLink.Text.Contains("cсылка для перехода"))
            {
                searchLink.Click();
            }
            else
            {
                ScenarioContext.Current.Pending();
            }
        }

        [Then(@"я поподаю на профиль пользователя и вижу подтвержденную почту почты")]
        public void ТоЯПоподаюНаПрофильПользователяИВижуПодтвержденнуюПочтуПочты()
        {
            Thread.Sleep(2000);
            WebDriverManager.GetDriver().SwitchTo().DefaultContent();
            IWebElement searchElement = WebDriverManager.GetDriver().FindElement(By.CssSelector("p"));
            Assert.IsTrue(searchElement.Text.Contains("\r\n        Благодарим вас за подтверждение вашей электронной почты.\r\n    "));
        }
    }
}
