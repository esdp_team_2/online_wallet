﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OpenQA.Selenium.Remote;
using System.Configuration;
using Online_wallet_project.AcceptanceTests.Models;

namespace Online_wallet_project.AcceptanceTests.Steps
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base() 
        {
            
        }

        public DbSet<User> AspNetUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["PublickDatabase"].ConnectionString);
        //}
    }
}
