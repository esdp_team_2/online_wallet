﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Models.GroupViewModel;
using Online_wallet_project.Models.ManagerViewModels;
using Online_wallet_project.Models.SupplierUserViewModel;
using Online_wallet_project.Models.SupplierViewModel;
using Online_wallet_project.Models.ViewModels;
using Online_wallet_project.Services;


namespace Online_wallet_project.Controllers
{
    [Authorize(Roles = "Manager")]
    [Route("[controller]/[action]")]
    public class ManagerController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;
        private readonly PagingService _paging;


        public ManagerController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            ILogger<AccountController> logger,
            IHostingEnvironment environment,
            FileUploadService fileUploadService,
            PagingService paging)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _logger = logger;
            _environment = environment;
            _fileUploadService = fileUploadService;
            _paging = paging;
        }

        public IActionResult Index(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            // Очистите существующий внешний файл cookie, чтобы обеспечить чистый процесс входа в систему
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(ManagerLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await ForValidationUsersAndAdmin.GetUserRolesOnRole(_userManager, "Manager", model.Login))
                {
                    var result =
                        await _signInManager.PasswordSignInAsync(model.Login, model.Password, false,
                            lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("Manager logged in.");
                        return RedirectToAction("Index", "Manager");
                    }

                    ModelState.AddModelError(string.Empty, "Неправильный логин и/или пароль.");
                    return View(model);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Неправильный логин и/или пароль");
                    return View(model);
                }
            }
            return View(model);
        }


        #region ForSupplier
        // Отчеты по поставщикам
        [HttpGet]
        public IActionResult ReportOfSupplier()
        {
            SelectList supplier = new SelectList(_context.Suppliers, "Id", "SupplierName");
            if (supplier.Any())
            {
                ViewBag.Supplier = supplier;
                return View();
            }
            else
            {
                return View("SupReportErrore");
            }
        }

        [HttpGet]
        public IActionResult AllReportOfSupplier(DateTime? dateTimeFrom, DateTime? dateTimeTo,
            decimal? sumFrom, decimal? sumTo, int supplierId, int page = 1)
        {
            IQueryable<Transaction> transactions = _context.Transactions
                .Where(t => t.SupplierId == supplierId)
                .Include(t => t.Status)
                .Where(t => t.Status.StatusName == "Pay" || t.Status.StatusName == "Returne");

            if (transactions.Any() && dateTimeFrom != null || dateTimeTo != null || sumFrom != null || sumTo != null)
            {
                transactions =
                    ForFiltrAndSort.FilterForTransaction(dateTimeTo, dateTimeFrom, sumFrom, sumTo, transactions);

                // пэйджинг
                PagedObject<Transaction> pagedObject = _paging.DoPage(transactions, page);

                SupplierPagingViewModel viewModel = new SupplierPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, sumFrom, sumTo),
                    Transactions = pagedObject.Objects,
                    SupplierId = supplierId
                };

                return View(viewModel);
            }
            else
            {
                transactions.Where(t => t.DateOfCreate.Month == DateTime.Now.Month);
                PagedObject<Transaction> pagedObject = _paging.DoPage(transactions, page);

                SupplierPagingViewModel viewModel = new SupplierPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, sumFrom, sumTo),
                    Transactions = pagedObject.Objects,
                    SupplierId = supplierId
                };
                return View(viewModel);
            }
        }

        [HttpGet]
        public IActionResult AllProfitOfSupplier(int supplierId, DateTime? dateTimeTo,
            DateTime? dateTimeFrom, int page = 1)
        {
            SupplierFeeStatusChange status =
              _context.SupplierFeeStatusChanges.FirstOrDefault(s => s.SupplierId == supplierId);

            string queryString = $"Select Sum(SumPay) as 'SumPay', SUM(SumFee) as 'SumFee'," +
                                 $" Format(Min(DateOfCreate),'MM/yyyy','ru-RU') as 'DateOfTime'" +
                                 $" from Transactions where StatusId = 2" +
                                 $" group by DATEPART(yyyy, DateOfCreate), DATEPART(mm, DateOfCreate)";


            List<SupplierUserGroupingTransaction> model = ForSqlQuery.SqlQueryMethod(queryString);
            List<SupplierUserGroupingTransaction> newModel = new List<SupplierUserGroupingTransaction>();

            if (status.FeeStatus == SupplierFeeStatusModel.NoCommission)
            {
                IEnumerable<SuppliersTurnoverReport> report =
                    _context.SuppliersTurnoverReports.Where(t => t.SupplierId == supplierId);


                foreach (SupplierUserGroupingTransaction groupingTransaction in model)
                {
                    decimal profit = 0;

                    foreach (SuppliersTurnoverReport r in report)
                    {
                        if (r.CalculatingMethod == CalculatingMethodModel.Percent)
                        {
                            profit += groupingTransaction.AllSum * r.SharedValue / 100;
                        }
                        else
                        {
                            if (r.SumMin <= groupingTransaction.AllSum && r.SumMax >= groupingTransaction.AllSum)
                            {
                                profit += r.SharedValue;
                            }
                        }
                    }

                    newModel.Add(new SupplierUserGroupingTransaction
                    {
                        AllFee = groupingTransaction.AllFee,
                        AllSum = groupingTransaction.AllSum,
                        Date = groupingTransaction.Date,
                        Profit = profit
                    });
                }

                if (dateTimeTo != null || dateTimeFrom != null)
                {
                    newModel = ForFiltrAndSort.FilterForProfitTransaction(dateTimeTo, dateTimeFrom, newModel);
                }

                PagedObject<SupplierUserGroupingTransaction> pagedObject = _paging.DoPage(newModel.AsQueryable(), page);

                SupplierUserShowProfitPagingViewModel viewModel = new SupplierUserShowProfitPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, null, null),
                    SupplierUserGroupingTransactions = pagedObject.Objects.OrderByDescending(t => t.Date.Year).ToList(),
                    SupplierId = supplierId
                };

                return View(viewModel);
            }
            else
            {
                IEnumerable<FeePartReport> report =
                   _context.FeePartReports.Where(t => t.SupplierId == supplierId);


                foreach (SupplierUserGroupingTransaction groupingTransaction in model)
                {
                    decimal profit = 0;

                    foreach (FeePartReport r in report)
                    {
                        if (r.CalculatingMethod == CalculatingMethodModel.Percent)
                        {
                            profit += groupingTransaction.AllFee * r.SharedValue / 100;
                        }
                    }

                    newModel.Add(new SupplierUserGroupingTransaction
                    {
                        AllFee = groupingTransaction.AllSum,
                        AllSum = groupingTransaction.AllFee,
                        Date = groupingTransaction.Date,
                        Profit = profit
                    });
                }

                if (dateTimeTo != null || dateTimeFrom != null)
                {
                    newModel = ForFiltrAndSort.FilterForProfitTransaction(dateTimeTo, dateTimeFrom, newModel);
                }

                PagedObject<SupplierUserGroupingTransaction> pagedObject = _paging.DoPage(newModel.AsQueryable(), page);

                SupplierUserShowProfitPagingViewModel viewModel = new SupplierUserShowProfitPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, null, null),
                    SupplierUserGroupingTransactions = pagedObject.Objects.OrderByDescending(t => t.Date.Year).ToList(),
                    SupplierId = supplierId
                };

                return View(viewModel);
            }
        }

        //насройки соединения с Api dll
        [HttpGet]
        public IActionResult ConnectionSettings(SupplierConnectionSettingViewModel model)
        {
            if (!string.IsNullOrEmpty(model.ErrorMassage)) ViewBag.Message = model.ErrorMassage;

            return View(model);
        }

        [HttpGet]
        public IActionResult ConnectionValueSetting(SupplierConnectionSettingViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ConnectionSetting(SupplierConnectionSettingViewModel model, SupplierFieldValueViewModel secondModel)
        {
            if (secondModel.FieldNames == null)
            {
                if (model.FieldName == null || model.DllSup == null)
                {
                    model.ErrorMassage = "* Вы должны создать хоть одно динамическое поле и вы должны загрузить dll библиотеку";
                    return RedirectToAction("ConnectionSettings", model);
                }

                string json = SerializeInDictionary(model.FieldName, model.FieldType);

                _context.SupplierConfigConnections.Add(new SupplierConfigConnection
                {
                    SupplierId = model.SupplierId,
                    FieldData = json,
                    DllPath = Upload(model.SupplierId, model.DllSup,"library","dll","supplier")
                });

                _context.SaveChanges();
                return RedirectToAction("ConnectionValueSetting",model);
            }
            else
            {
                SupplierConfigConnection config =
                    _context.SupplierConfigConnections.Include(s => s.Supplier).FirstOrDefault(s =>
                        s.SupplierId == secondModel.SupplierIdSecond);

                config.ConnectionValue = SerializeInDictionary(secondModel.FieldNames, secondModel.FieldValues);
                _context.SupplierConfigConnections.Update(config);
                _context.SaveChanges();

                return RedirectToAction("FeeTypeSelect", "Fee", new
                {
                    supplierId = config.Supplier.Id,
                    message =
                    $"Поставщик {config.Supplier.SupplierName} создан успешно. Настройте комиссию для поставщика"
                });
            }
        }

        private string SerializeInDictionary(string[] FirstKey, string[] SecondValue)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            for (int i = 0; i < FirstKey.Length; i++)
            {
                dictionary.Add(FirstKey[i], SecondValue[i]);
            }
            return JsonConvert.SerializeObject(dictionary);
        }

        private string Upload(int supplierId, IFormFile file, string folder, string secondFolder, string thirdFolder)
        {
            var path = Path.Combine(
                _environment.WebRootPath,
                $"{folder}\\{thirdFolder}\\{supplierId}\\{secondFolder}");
            _fileUploadService.Upload(path, file.FileName, file);

            return $"{folder}/{thirdFolder}/{supplierId}/{secondFolder}/{file.FileName}";
        }

        public async Task<IActionResult> UnBlockSupplier(int id)
        {
            SupplierViewModelForBlockAndUnBlock model = await ForBlockAndUnblock(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UnBlockSupplier(SupplierViewModelForBlockAndUnBlock model)
        {
            string nameSupplier = await MethodForWriteInReportBlockAndUnBlock(model, false);
            return RedirectToAction("ShowAllSuppliers", new { message = $"Поставщик {nameSupplier} разблокирован" });
        }

        public async Task<IActionResult> BlockSupplier(int id)
        {
            SupplierViewModelForBlockAndUnBlock model = await ForBlockAndUnblock(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BlockSupplier(SupplierViewModelForBlockAndUnBlock model)
        {
            string nameSupplier = await MethodForWriteInReportBlockAndUnBlock(model, true);
            return RedirectToAction("ShowAllSuppliers", new {message = $"Поставщик {nameSupplier} заблокирован"});
        }

        [NonAction]
        private async Task<string> MethodForWriteInReportBlockAndUnBlock(SupplierViewModelForBlockAndUnBlock model, bool block)
        {
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == model.SupplierID);
            supplier.Blocking = block;
            BlockedSupplier blockedSupplier = new BlockedSupplier
            {
                SupplierId = supplier.Id,
                ManagerId = model.ManagerID,
                Description = model.Description,
                Date = DateTime.Now
            };
            _context.UpdateRange(supplier, blockedSupplier);
            await _context.SaveChangesAsync();
            return supplier.SupplierName;
        }

        [NonAction]
        private async Task<SupplierViewModelForBlockAndUnBlock> ForBlockAndUnblock(int suplierId)
        {
            ApplicationUser manager = await _userManager.FindByNameAsync(User.Identity.Name);
            return new SupplierViewModelForBlockAndUnBlock
            {
                SupplierID = suplierId,
                ManagerID = manager.Id
            };
        }

        public IActionResult ShowAllSuppliers(string message)
        {
            ViewBag.Message = message;
            IEnumerable<SupplierModel> allSuppliers = _context.Suppliers.Include(s => s.GroupModel);
            return View(allSuppliers);
        }
        
        public IActionResult CreateSupplier(string message)
        {
            ViewBag.Message = message;
            ViewBag.GroupModel = new SelectList(_context.GroupModels, "Id", "GroupName");
            SupplierRegisterViewModel model = new SupplierRegisterViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateSupplier(SupplierRegisterViewModel model)
        {
            ViewBag.GroupModel = new SelectList(_context.GroupModels, "Id", "GroupName");
            if (model.IsRestrictionUpToInfinity)
            {
                model.AmountOfRestriction = UInt32.MaxValue;
            }
            if (ModelState.IsValid)
            {
                if (model.FieldName == null)
                {
                    ModelState.AddModelError(string.Empty,"Вы должны создать хоть одно динамическое поле!");
                    return View(model);
                }

                string json = ForSerializeFields(model.FieldName, model.FieldType, model.FieldLenghtData);

                if (await TrueOrFalseUniqueSupplierNameAndPersonalAccount(model.SupplierName, model.PersonalAccount))
                {
                    _context.Suppliers.Add(new SupplierModel
                    {
                        SupplierName = model.SupplierName,
                        PersonalAccount = model.PersonalAccount,
                        BranchCode = model.BranchCode,
                        GroupModelId = model.GroupModelId,
                        FieldData = json,
                        AmountOfRestriction = model.AmountOfRestriction
                    });

                    _context.SaveChanges();
                    SupplierModel supplier =
                        _context.Suppliers.FirstOrDefault(g => g.SupplierName == model.SupplierName);

                    _logger.LogInformation($"group {supplier.SupplierName} registered");

                    if (ValidateOnDefaultGroupLogo(model.Logo, null, supplier))
                    {
                        supplier.Logo = Upload(supplier.Id, model.Logo, "images", "avatar","supplier");
                    }
                    _context.Suppliers.Update(supplier);
                    await _context.SaveChangesAsync();
                    _logger.LogInformation("Manager created a new Supplier.");


                    return RedirectToAction("ConnectionSettings", new{ supplierId = supplier.Id});
                }
            }
            
            return View(model);
        }

        [NonAction]
        private string ForSerializeFields(string[] fieldName, string[] fieldType, int[] fieldLenghtData)
        {
            List<FieldTemplate> filds = new List<FieldTemplate>();
            for (int i = 0; i <= fieldName.Length - 1; i++)
            {
                filds.Add(new FieldTemplate { FieldLenghtData = fieldLenghtData[i], FieldName = fieldName[i], FieldType = fieldType[i] });
            }
            return JsonConvert.SerializeObject(filds);
        }

        [HttpGet]
        public async Task<IActionResult> EditSupplier(int Id)
        {
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == Id);
            SupplierViewModelForFields model = new SupplierViewModelForFields
            {
                Supplier = supplier,
                FieldTemplates = DeserializeJsonForFieldsTamplate(supplier.FieldData),
            };
            
            ViewBag.GroupModel = new SelectList(_context.GroupModels, "Id", "GroupName");
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditSupplier(SupplierModel supplier, IFormFile Logo, string[] fieldName, string[] fieldType, int[] fieldLenghtData)
        {
            ViewBag.GroupModel = new SelectList(_context.GroupModels, "Id", "GroupName");
            if (ModelState.IsValid)
            {
                if (await TrueOrFalseUniqueSupplierNameAndPersonalAccount(supplier.SupplierName, supplier.PersonalAccount, supplier.Id))
                {
                    if (Logo != null)
                    {
                        supplier.Logo = Upload(supplier.Id, Logo, "images", "avatar","supplier");
                    }

                    supplier.FieldData = ForSerializeFields(fieldName, fieldType, fieldLenghtData);

                    _context.Suppliers.Update(supplier);
                    _context.SaveChanges();
                    return RedirectToAction("Index", "Manager", new { message = $"Поставщик {supplier.SupplierName} успешно изменен" });
                }
                else
                {
                    return View(new SupplierViewModelForFields
                    {
                        Supplier = supplier,
                        FieldTemplates = DeserializeJsonForFieldsTamplate(supplier.FieldData)
                    });
                }
            }
            return View(new SupplierViewModelForFields
            {
                Supplier = supplier,
                FieldTemplates = DeserializeJsonForFieldsTamplate(supplier.FieldData)
            });
        }

        [HttpGet]
        public async Task<IActionResult> EditConnectionSettings(int Id)
        {
            SupplierConfigConnection configConnection = await 
                _context.SupplierConfigConnections.FirstOrDefaultAsync(s =>s.SupplierId == Id);
            SupplierViewModelForConnectionSetting model = new SupplierViewModelForConnectionSetting
            {
                SupplierId = configConnection.SupplierId,
                DllPath = configConnection.DllPath,
                ConnectionValue = JsonConvert.DeserializeObject<Dictionary<string, string>>(configConnection.ConnectionValue)
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditConnectionSettings(int SupplierId, string[] FieldNames, string[] FieldValues, IFormFile DllSup)
        {
            SupplierConfigConnection editConfigConnection = await
                _context.SupplierConfigConnections.Include(s=>s.Supplier).FirstOrDefaultAsync(s => s.SupplierId == SupplierId);
            if (ModelState.IsValid)
            {
                if (DllSup != null)
                {
                    editConfigConnection.DllPath = Upload(SupplierId, DllSup, "library", "dll", "supplier");
                    
                }

                editConfigConnection.ConnectionValue = SerializeInDictionary(FieldNames, FieldValues);
                _context.SupplierConfigConnections.Update(editConfigConnection);
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Manager", new { message = $"Поставщик {editConfigConnection.Supplier.SupplierName} успешно изменен" });
        }

        public IActionResult CreateUserSupplier()
        {
            ViewBag.SupplierModel = new SelectList(_context.Suppliers, "Id", "SupplierName");
            return View();
        } 

        [HttpPost]
        public async Task<IActionResult> CreateUserSupplier(CreateSupplierUserVIewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.SupplierModelId == 0)
                {
                    ModelState.AddModelError(String.Empty, "Вы не можете создать пользователя для поставщика, так как в системе нету поставщиков!");
                    return View(model);
                }

                var user = new ApplicationUser
                {
                    UserName = model.Login,
                    DateOfCreation = DateTime.Now,
                    SupplierId = model.SupplierModelId
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //Присвоивание пользователю при регистрации роли "Supplier"
                    IdentityRole role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == "Supplier");
                    await _context.UserRoles.AddAsync(new IdentityUserRole<string>
                    {
                        RoleId = role.Id,
                        UserId = user.Id
                    });
                    await _context.SaveChangesAsync();

                    _logger.LogInformation("Manager created a new UserSupplier with password.");
                    return RedirectToAction("Index", "Manager",
                        new { message = $"Пользователь-поставщик {model.Login} с паролем {model.Password} создан!" });
                }

            }
            ViewBag.SupplierModel = new SelectList(_context.Suppliers, "Id", "SupplierName");
            ModelState.AddModelError(string.Empty,"Логин не должен сожержать пробелов");
            return View(model);
        }

        #endregion

        #region ForGroup

        public IActionResult CreateGroup(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateGroup(GroupRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await TrueOrFalseUniqeGroupName(model.GroupName))
                {
                    _context.Add(new GroupModel { GroupName = model.GroupName });
                    _context.SaveChanges();
                    GroupModel groupModel = _context.GroupModels.FirstOrDefault(g => g.GroupName == model.GroupName);

                    _logger.LogInformation($"group {groupModel.GroupName} registered");


                    if (ValidateOnDefaultGroupLogo(model.GroupLogo, groupModel))
                    {
                        groupModel.GroupLogo = Upload(groupModel.Id, model.GroupLogo, "images", "avatar", "group");
                    }

                    _context.GroupModels.Update(groupModel);
                    await _context.SaveChangesAsync();
                    _logger.LogInformation("Manager created a new Group.");
                    return RedirectToAction("Index", "Manager",
                        new { message = $"Группа {groupModel.GroupName} созданна успешно" });
                }
                else
                {
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult ShowAllGroups()
        {
            IEnumerable<GroupModel> allGroups = _context.GroupModels;
            return View(allGroups);
        }

        [HttpGet]
        public async Task<IActionResult> EditGroup(int id)
        {
            GroupModel group = await _context.GroupModels.FirstOrDefaultAsync(u => u.Id == id);
            return View(group);
        }

        [HttpPost]
        public IActionResult EditGroup(GroupModel group, IFormFile groupLogo)
        {
            if (groupLogo != null)
            {
                group.GroupLogo = Upload(group.Id, groupLogo, "images", "avatar", "group");
            }

            _context.GroupModels.Update(group);
            _context.SaveChanges();
            return RedirectToAction("Index", "Manager", new { message = "Группа успешно изменена" });
        }

        #endregion

        #region ForValidation

        [NonAction]
        private bool ValidateOnDefaultGroupLogo(IFormFile model = null, GroupModel groupModel = null, SupplierModel supplierModel = null)
        {
            if (groupModel != null && model == null)
            {
                groupModel.GroupLogo = $"images/DefaultLogos/DefaultGroupLogo.png";
                return false;
            }
            if (model == null && supplierModel != null)
            {
                supplierModel.Logo = $"images/DefaultLogos/DefaultGroupLogo.png";
                return false;
            }
            else
            {
                return true;
            }
        }

        [NonAction]
        private async Task<bool> TrueOrFalseUniqeGroupName(string groupName)
        {
            GroupModel group = await _context.GroupModels.FirstOrDefaultAsync(u => u.GroupName == groupName);
            if (group == null)
            {
                return true;
            }
            else
            {
                ModelState.AddModelError(String.Empty, $"Группа с названием {groupName} уже существует");
                return false;
            }
        }

        [NonAction]
        private async Task<bool> TrueOrFalseUniqueSupplierNameAndPersonalAccount(string SupplierName, string PersonalAccount, int id = 0)
        {
            bool result = true;
            if (!string.IsNullOrEmpty(SupplierName))
            {
                SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(u => u.SupplierName == SupplierName && u.Id != id);
                if (supplier != null)
                {
                    ModelState.AddModelError(String.Empty, $"Поставщик с названием {SupplierName} уже существует");
                    result = false;
                }
            }
            if (!string.IsNullOrEmpty(PersonalAccount))
            {
                SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(u => u.PersonalAccount == PersonalAccount && u.Id != id);
                if (supplier != null)
                {
                    ModelState.AddModelError(String.Empty, $"Поставщик с лицевым счетом {PersonalAccount} уже существует");
                    result = false;
                }
            }

            return result;

        }

        [NonAction]
        private FieldTemplate[] DeserializeJsonForFieldsTamplate(string Json)
        {
            return JsonConvert.DeserializeObject<FieldTemplate[]>(Json);
        }
        #endregion
    }
}