using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Models.ManageViewModels;
using Online_wallet_project.Models.ViewModels;
using Online_wallet_project.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Online_wallet_project.Models.SupplierUserViewModel;

namespace Online_wallet_project.Controllers
{
    [Authorize(Roles = "User")]
    [Route("[controller]/[action]")]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly UrlEncoder _urlEncoder;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;
        private readonly UserEmailChangeService _userEmailChange;
        private const string AuthenicatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private CustomValodatorPassword _customValodatorPassword;
        private readonly UserProfileChangeService _userProfileChange;
        private readonly UserPasswordChangeService _userPasswordChange;
        private readonly PagingService _pagingService;

        public ManageController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<ManageController> logger,
            UrlEncoder urlEncoder,
            IConfiguration configuration,
            ApplicationDbContext context,
            UserEmailChangeService userEmailChange,
            UserProfileChangeService userProfileChange,
            UserPasswordChangeService userPasswordChange,
            PagingService pagingService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _urlEncoder = urlEncoder;
            _context = context;
            _configuration = configuration;
            _customValodatorPassword = new CustomValodatorPassword(_context, _context.ValidationSettingses.FirstOrDefault().LengthPasswordInt);
            _userEmailChange = userEmailChange;
            _userProfileChange = userProfileChange;
            _userPasswordChange = userPasswordChange;
            _pagingService = pagingService;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [HttpGet]
        public async Task<IActionResult> Index(string message)
        {
            StatusMessage = message;

            await ForValidationUsersAndAdmin.CheckBlockedOrNot(_userManager, _signInManager, User.Identity.Name, "User");

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            IQueryable<Transaction> transactionsPre = _context.Transactions.Where(t => t.UserId == user.Id && t.StatusId != 6).
                OrderByDescending(t => t.DateOfCreate).Include(t => t.Supplier).Include(t => t.Status);
                      

            var model = new IndexViewModel
            {
                Username = user.UserName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                IsEmailConfirmed = user.EmailConfirmed,
                StatusMessage = StatusMessage,
                Balance = user.MoneyBalance,
                FirstName = user.FirstName,
                Lastname = user.LastName,
                LastTransactions = transactionsPre.GroupBy(t => t.GeneratedId).Select(t => t.First()).Take(5)
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!model.IsEmailConfirmed)
            {
                model.StatusMessage = "Error:Неподтвержденная почта. Вы не сможете редактировать свои данные";
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            StatusMessage = await _userProfileChange.UpdateUserProfileViaEmail(user, model, Url, Request);

            return RedirectToAction(nameof(Index), new { message = StatusMessage });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendVerificationEmail(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
            var email = user.Email;
            _emailSender.SendEmailConfirmationAsync(email, callbackUrl);

            StatusMessage = "На вашу почту отправлена ссылка. Пройдите по ссылке для подтверждения почты.";
            return View("_StatusMessage", StatusMessage);
        }

        #region ChangePassword

        [HttpGet]
        public async Task<IActionResult> SendChangePasswordLinkToEmail()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendChangePasswordLinkToEmail(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Не удалось загрузить пользователя с идентификатором '{_userManager.GetUserId(User)}'");
            }

            _userPasswordChange.SendChangePasswordLinkAsync(user, Url, Request);

            StatusMessage = "Вам на почту была отправлена ссылка. Перейдите по ссылке для смены пароля";
            return View("_StatusMessage", StatusMessage);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ChangePassword(string userId, string code, string message)
        {
            if (userId == null || code == null)
            {
                return FailedChangePassword();
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.FindByIdAsync(userId)}'.");
            }

            StatusMessage = message;
            var model = new ChangePasswordViewModel { UserId = userId, Code = code, StatusMessage = StatusMessage};
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByIdAsync(model.UserId);
            var oldPassword = user.PasswordHash;
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.FindByIdAsync(model.UserId)}'.");
            }

            if (!await _userManager.CheckPasswordAsync(user, model.OldPassword))
            {
                model.StatusMessage = "Error:Неверный текущий пароль";
                return View(model);
            }

            var resultValidatorPassword = await _customValodatorPassword.ValidateAsync(model.NewPassword);
            if (resultValidatorPassword.Succeeded)
            {
                var changePasswordResult = await _userManager.ResetPasswordAsync(user, model.Code, model.NewPassword);
                if (!changePasswordResult.Succeeded)
                {
                    return FailedChangePassword();
                }

                const string reason = "Смена пароля";
                _userPasswordChange.WriteInPasswordChangeAsync(user.Id, oldPassword, user.PasswordHash, reason);

                _logger.LogInformation("User changed password successfully");
                model.StatusMessage = "Ваш пароль успешно изменен. Попробуйте залогиниться";

                if (User.Identity.IsAuthenticated)
                {
                    await _signInManager.SignOutAsync();
                }
            }
            else
            {
                foreach (var error in resultValidatorPassword.Errors)
                {
                    ModelState.AddModelError(String.Empty, error.Description);
                }
            }
            return View(model);
        }
        #endregion

        #region SetPassword

        [HttpGet]
        public async Task<IActionResult> SetPassword()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var hasPassword = await _userManager.HasPasswordAsync(user);

            if (hasPassword)
            {
                return RedirectToAction(nameof(ChangePassword));
            }

            var model = new SetPasswordViewModel { StatusMessage = StatusMessage };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var addPasswordResult = await _userManager.AddPasswordAsync(user, model.NewPassword);
            if (!addPasswordResult.Succeeded)
            {
                AddErrors(addPasswordResult);
                return View(model);
            }

            await _signInManager.SignInAsync(user, isPersistent: false);
            StatusMessage = "Your password has been set.";

            return RedirectToAction(nameof(SetPassword));
        }
        #endregion

        [HttpGet]
        public async Task<IActionResult> ExternalLogins()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new ExternalLoginsViewModel { CurrentLogins = await _userManager.GetLoginsAsync(user) };
            model.OtherLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync())
                .Where(auth => model.CurrentLogins.All(ul => auth.Name != ul.LoginProvider))
                .ToList();
            model.ShowRemoveButton = await _userManager.HasPasswordAsync(user) || model.CurrentLogins.Count > 1;
            model.StatusMessage = StatusMessage;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LinkLogin(string provider)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            // Request a redirect to the external login provider to link a login for the current user
            var redirectUrl = Url.Action(nameof(LinkLoginCallback));
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl, _userManager.GetUserId(User));
            return new ChallengeResult(provider, properties);
        }

        [HttpGet]
        public async Task<IActionResult> LinkLoginCallback()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var info = await _signInManager.GetExternalLoginInfoAsync(user.Id);
            if (info == null)
            {
                throw new ApplicationException($"Unexpected error occurred loading external login info for user with ID '{user.Id}'.");
            }

            var result = await _userManager.AddLoginAsync(user, info);
            if (!result.Succeeded)
            {
                throw new ApplicationException($"Unexpected error occurred adding external login for user with ID '{user.Id}'.");
            }

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            StatusMessage = "The external login was added.";
            return RedirectToAction(nameof(ExternalLogins));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveLogin(RemoveLoginViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var result = await _userManager.RemoveLoginAsync(user, model.LoginProvider, model.ProviderKey);
            if (!result.Succeeded)
            {
                throw new ApplicationException($"Unexpected error occurred removing external login for user with ID '{user.Id}'.");
            }

            await _signInManager.SignInAsync(user, isPersistent: false);
            StatusMessage = "The external login was removed.";
            return RedirectToAction(nameof(ExternalLogins));
        }

        [HttpGet]
        public async Task<IActionResult> TwoFactorAuthentication()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new TwoFactorAuthenticationViewModel
            {
                HasAuthenticator = await _userManager.GetAuthenticatorKeyAsync(user) != null,
                Is2faEnabled = user.TwoFactorEnabled,
                RecoveryCodesLeft = await _userManager.CountRecoveryCodesAsync(user),
            };

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Disable2faWarning()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Unexpected error occured disabling 2FA for user with ID '{user.Id}'.");
            }

            return View(nameof(Disable2fa));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Disable2fa()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var disable2faResult = await _userManager.SetTwoFactorEnabledAsync(user, false);
            if (!disable2faResult.Succeeded)
            {
                throw new ApplicationException($"Unexpected error occured disabling 2FA for user with ID '{user.Id}'.");
            }

            _logger.LogInformation("User with ID {UserId} has disabled 2fa.", user.Id);
            return RedirectToAction(nameof(TwoFactorAuthentication));
        }

        [HttpGet]
        public async Task<IActionResult> EnableAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            if (string.IsNullOrEmpty(unformattedKey))
            {
                await _userManager.ResetAuthenticatorKeyAsync(user);
                unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            }

            var model = new EnableAuthenticatorViewModel
            {
                SharedKey = FormatKey(unformattedKey),
                AuthenticatorUri = GenerateQrCodeUri(user.Email, unformattedKey)
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EnableAuthenticator(EnableAuthenticatorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            // Strip spaces and hypens
            var verificationCode = model.Code.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2faTokenValid = await _userManager.VerifyTwoFactorTokenAsync(
                user, _userManager.Options.Tokens.AuthenticatorTokenProvider, verificationCode);

            if (!is2faTokenValid)
            {
                ModelState.AddModelError("model.Code", "Verification code is invalid.");
                return View(model);
            }

            await _userManager.SetTwoFactorEnabledAsync(user, true);
            _logger.LogInformation("User with ID {UserId} has enabled 2FA with an authenticator app.", user.Id);
            return RedirectToAction(nameof(GenerateRecoveryCodes));
        }

        [HttpGet]
        public IActionResult ResetAuthenticatorWarning()
        {
            return View(nameof(ResetAuthenticator));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await _userManager.SetTwoFactorEnabledAsync(user, false);
            await _userManager.ResetAuthenticatorKeyAsync(user);
            _logger.LogInformation("User with id '{UserId}' has reset their authentication app key.", user.Id);

            return RedirectToAction(nameof(EnableAuthenticator));
        }

        [HttpGet]
        public async Task<IActionResult> GenerateRecoveryCodes()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Cannot generate recovery codes for user with ID '{user.Id}' as they do not have 2FA enabled.");
            }

            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10);
            var model = new GenerateRecoveryCodesViewModel { RecoveryCodes = recoveryCodes.ToArray() };

            _logger.LogInformation("User with ID {UserId} has generated new 2FA recovery codes.", user.Id);

            return View(model);
        }

        public IActionResult AddToFavorites(int id)
        {
            Transaction transaction = _context.Transactions.FirstOrDefault(t => t.Id == id);
            transaction.IsFavorite = true;
            _context.Update(transaction);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult ShowFavoriteTransactions(int page = 1)
        {
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            IQueryable<Transaction> transactions = _context.Transactions.Where(t => t.UserId == user.Id)
                .Where(t => t.IsFavorite == true).Include(t => t.Supplier);

            PagedObject<Transaction> pagedObject = _pagingService.DoPage(transactions, page);

            ManagePagingViewModel model = new ManagePagingViewModel
            {
                PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                Transactions = pagedObject.Objects
            };

            return View(model);
        }

        public IActionResult DeleteFromFavorites(int id)
        {
            Transaction transaction = _context.Transactions.FirstOrDefault(t => t.Id == id);
            transaction.IsFavorite = false;
            _context.Update(transaction);
            _context.SaveChanges();
            return RedirectToAction(nameof(ShowFavoriteTransactions));
        }

        #region HistoryTransaction

        public IActionResult HistoryTransaction(int page = 1)
        {
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            IQueryable<Transaction> transactions = _context.Transactions
                .Where(t => t.UserId == user.Id)
                .Include(t => t.Supplier)
                .Include(s => s.Status)
                .Where(t => t.Status.StatusName == "Pay" || t.Status.StatusName == "Returne");

            PagedObject<Transaction> pagedObject = _pagingService.DoPage(transactions, page);

            ManagePagingViewModel model = new ManagePagingViewModel
            {
                PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                Transactions = pagedObject.Objects
            };

            return View(model);
        }

        #endregion

        [HttpGet]
        public IActionResult SalesReceipt(int transactionId)
        {
            Transaction transaction = _context.Transactions.FirstOrDefault(t => t.Id == transactionId);
            StatusModel status = _context.StatusModels.FirstOrDefault(s => s.Id == transaction.StatusId);
            SupplierModel supplier = _context.Suppliers.FirstOrDefault(s => s.Id == transaction.SupplierId);
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.Id == transaction.UserId);
            string companyInfo = System.IO.File.ReadAllText("CompanyInfo.json");

            SalesReceiptViewModel model = new SalesReceiptViewModel
            {
                CompanyInfo = companyInfo,
                DateOfCreate = transaction.DateOfCreate.Year + "." + transaction.DateOfCreate.Month + "." + transaction.DateOfCreate.Day + "( " + transaction.DateOfCreate.Hour + ":" + transaction.DateOfCreate.Minute + " )",
                UserName = user.LastName + " " + user.FirstName,
                StatusName = status.StatusName,
                SumFee = transaction.SumFee.ToString(),
                SumPay = transaction.SumPay.ToString(),
                SumPayAndFee =(transaction.SumFee + transaction.SumPay).ToString(),
                SupplierName = supplier.SupplierName,
                TransactionId = transaction.Id.ToString(),
                PersonalAccount = transaction.ReportRequisite
            };
            return View(model);
        }

        #region ChangeEmail

        [HttpGet]
        public async Task<IActionResult> EmailChange()
        {
            var user = await _userManager.GetUserAsync(User);
            var model = new ChangeEmailViewModel {IsCurrentEmailConfirmed = user.EmailConfirmed};
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EmailChange(ChangeEmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (!await _emailSender.IsEmailExist(model.NewEmail))
            {
                model.StatusMessage = "Error:Такой почты не существует";
                return View(model);
            }
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await _userEmailChange.WriteInEmailChangeAsync(user.Id, user.Email, model.NewEmail, false);

            //get id of emailchange and put it into sendemailchange instead of model.newEmail
            await _userEmailChange.SendEmailChangeConfirmationAsync(user, model.NewEmail, Url, Request);

            _logger.LogInformation("User set new email.");
            
            model.StatusMessage = $"На вашу текущую почту \"{user.Email}\" отправлена ссылка о подтверждении смены почты. \nПройдите по ссылке";
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmailChange(string userId, string code, string newEmail)
        {
            if (userId == null || code == null || newEmail == null)
            {
                return FailedEmailChange();
            }

            var user = await _userManager.FindByIdAsync(userId);
            var userOldEmail = user.Email;

            if (user == null)
            {
                return View("Error");
            }

            //get newEmail form emailchanges via id
            var result = await _userManager.ChangeEmailAsync(user, newEmail, code);

            if (result.Succeeded)
            {
                await _userEmailChange.WriteInEmailChangeAsync(user.Id, userOldEmail, newEmail, true);

                await _userEmailChange.SendNewEmailConfirmationAsync(user, Url, Request);

                _logger.LogInformation("User changed email succesfully.");

                StatusMessage = $"Смена почты подтверждена. На вашу новую почту \"{user.Email}\" отправлена ссылка. \nДля подтверждения новой почты пройдите по ссылке.";

                if (User.Identity.IsAuthenticated)
                {
                    await _signInManager.SignOutAsync();
                }
                return View("_StatusMessage", StatusMessage);
            }
            return FailedEmailChange();
        }
        #endregion

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private string FormatKey(string unformattedKey)
        {
            var result = new StringBuilder();
            int currentPosition = 0;
            while (currentPosition + 4 < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
                currentPosition += 4;
            }
            if (currentPosition < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition));
            }

            return result.ToString().ToLowerInvariant();
        }

        private string GenerateQrCodeUri(string email, string unformattedKey)
        {
            return string.Format(
                AuthenicatorUriFormat,
                _urlEncoder.Encode("Online_wallet_project"),
                _urlEncoder.Encode(email),
                unformattedKey);
        }

        private IActionResult FailedEmailChange()
        {
            StatusMessage = "Error:Неудачная попытка смены почты. Попробуйте еще раз";
            return View("_StatusMessage", StatusMessage);
        }

        private IActionResult FailedChangePassword()
        {
            StatusMessage = "Error:Неудачная попытка смены пароля. Попробуйте еще раз";
            return View("_StatusMessage", StatusMessage);
        }
        #endregion
    }
}