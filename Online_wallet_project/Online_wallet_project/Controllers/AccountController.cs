﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Models.AccountViewModels;
using Online_wallet_project.Services;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Online_wallet_project.Controllers
{
    //[Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserLockOutSettings _userLockOutSettings;
        private readonly BlockedEverUsersService _blockedEverUsers;
        private CustomValodatorPassword _customValodatorPassword;
        private readonly UserPasswordChangeService _userPasswordChange;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context,
            IOptions<UserLockOutSettings> userLockOutSettings,
            BlockedEverUsersService blockedEverUsers,
            UserPasswordChangeService userPasswordChange)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
            _userLockOutSettings = userLockOutSettings.Value;
            _blockedEverUsers = blockedEverUsers;
            _customValodatorPassword = new CustomValodatorPassword(_context,
                _context.ValidationSettingses.FirstOrDefault().LengthPasswordInt);
            _userPasswordChange = userPasswordChange;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            
            ViewData["ReturnUrl"] = returnUrl;
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                string message = string.Empty;

                if (await ForValidationUsersAndAdmin.GetUserRolesOnRole(_userManager, "BlockedUser", model.Login))
                {
                    message = "Вы заблокированы администратором.";
                    return RedirectToAction(nameof(Lockout), new { message });
                }

                if (await ForValidationUsersAndAdmin.GetUserRolesOnRole(_userManager, "User", model.Login))
                {
                    var user = await _userManager.FindByNameAsync(model.Login);
                    bool lockoutOnFailure = _userLockOutSettings.UserLockoutEnabledByDefault.GetValueOrDefault();

                    // This doesn't count login failures towards account lockout
                    // To enable password failures to trigger account lockout, change to shouldLockout: true
                    var result = await _signInManager.PasswordSignInAsync(user, model.Password, isPersistent: false,
                        lockoutOnFailure: lockoutOnFailure);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User logged in.");
                        await _userManager.ResetAccessFailedCountAsync(user);

                        //разблокировка системой
                        if (DateTime.Now >= await _userManager.GetLockoutEndDateAsync(user))
                        {
                            _blockedEverUsers.UnWriteUserInBlockedEverUsers(user);
                        }
                        return RedirectToAction("Index", "Manage");
                    }
                    if (result.RequiresTwoFactor)
                    {
                        return RedirectToAction(nameof(LoginWith2fa), new { });
                    }
                    if (result.IsLockedOut)
                    {
                        message =
                            "Временная блокировка из-за превышение макс. количества " +
                            "попыток неверного входа. Повторите попытку входа позже.";

                        return RedirectToAction(nameof(Lockout), new {message});
                    }
                    if (!result.Succeeded)
                    {
                        int maxAttempts =
                            _userLockOutSettings.MaxFailedAccessAttempts.GetValueOrDefault();
                        int attemptsLeft = maxAttempts - user.AccessFailedCount;

                        if (user.AccessFailedCount >= maxAttempts)
                        {
                            _blockedEverUsers.WriteUserInBlockedEverUsers(user);
                            _logger.LogWarning("User account locked out timely.");
                        }

                        ModelState.AddModelError(string.Empty,
                            string.Format("Неверная попытка входа. Попыток осталось {0}.", attemptsLeft));
                        model.StatusMessage = $"Error:Неверная попытка входа. Попыток осталось { attemptsLeft}.";
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty,"Неправильный логин и/или пароль");
                    model.StatusMessage = "Error:Неправильный логин и/или пароль";
                    return View(model);
                }   
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var model = new LoginWith2faViewModel {RememberMe = rememberMe};
            ViewData["ReturnUrl"] = returnUrl;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe,
            string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result =
                await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe,
                    model.RememberMachine);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model,
            string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout(string message)
        {
            return View("Lockout", message);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!await _emailSender.IsEmailExist(model.Email))
                {
                    ModelState.AddModelError(string.Empty, $"Такой почты \"{model.Email}\" не существует");
                    return View(model);
                }

                //Настраиваимая валидация из Админки
                var resultValidation = await _customValodatorPassword.ValidateAsync(model.Password);
                if (resultValidation.Errors.Any())
                {
                    foreach (var error in resultValidation.Errors)
                    {
                        ModelState.AddModelError(String.Empty, error.Description);
                    }

                    return View(model);
                }

                var user = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    DateOfCreation = DateTime.Now
                };

                if (await ValidateForLoginAndEmail(user)) // проверка на уникальность только "User" пользователей
                {
                    return View(model);
                }

                 var result = await _userManager.CreateAsync(user,model.Password);
                if (result.Succeeded)
                {
                    //Присвоивание пользователю при регистрации роли "User"
                    IdentityRole role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == "User");
                    await _context.UserRoles.AddAsync(new IdentityUserRole<string>
                    {
                        RoleId = role.Id,
                        UserId = user.Id
                    });
                    await _context.SaveChangesAsync();

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                    var email = user.Email;
                    await _emailSender.SendEmailConfirmationAsync(email, callbackUrl);

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation("User created a new account with password.");

                    return RedirectToAction("Index", "Manage", new {message = "На вашу почту отправлена ссылка. Пройдите по ссылке для подтверждения почты." });
                }
                AddErrors(result);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction("SignedOut");
        }

        public IActionResult SignedOut()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new {returnUrl});
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToAction(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey,
                isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                return View("ExternalLogin", new ExternalLoginViewModel {Email = email});
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model,
            string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser {UserName = model.Email, Email = model.Email};
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);

            _logger.LogInformation("User confirmed email succesfully");
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        #region ForgotPassword

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Не показывайте, что пользователь не существует или не подтвержден
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }

                _userPasswordChange.SendForgotPasswordLinkAsync(user, user.Email, Url, Request);

                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return FailedResetPassword();
            }

            var user = await _userManager.FindByIdAsync(userId);
            var model = new ResetPasswordViewModel {Email = user.Email, Code = code};
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            var oldPassword = user.PasswordHash;
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }

            var resultValidatorPassword = await _customValodatorPassword.ValidateAsync(model.Password);
            if (resultValidatorPassword.Succeeded)
            {
                var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
                if (result.Succeeded)
                {
                    const string reason = "Сброс пароля";
                    await _userPasswordChange.WriteInPasswordChangeAsync(user.Id, oldPassword, user.PasswordHash, reason);

                    return RedirectToAction(nameof(ResetPasswordConfirmation));
                }
                AddErrors(result);
            }
            else
            {
                foreach (var error in resultValidatorPassword.Errors)
                {
                    ModelState.AddModelError(String.Empty, error.Description);
                }
                return View(model);
            }
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        private IActionResult FailedResetPassword()
        {
            string statusMessage = "Error:Неудачная попытка сброса пароля. Попробуйте еще раз";
            return View("_StatusMassageForAll", statusMessage);
        }

        #endregion

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region ForAccounValidations

        [NonAction]
        private async Task<bool> ValidateForLoginAndEmail(ApplicationUser user)
        {
            bool result = false;

            if (!await TrueOrFalseUniqeLoginNameOrGmail(user.UserName, null))
            {
                ModelState.AddModelError(string.Empty, "Такой пользователь с таким \"Логином\" существует");
                result = true;
            }
            if (!await TrueOrFalseUniqeLoginNameOrGmail(null, user.Email))
            {
                ModelState.AddModelError(string.Empty, "Такой пользователь с такой \"Почтой\" существует");
                result = true;
            }

            return result;
        }

        [NonAction]
        private async Task<bool> TrueOrFalseUniqeLoginNameOrGmail(string userName, string email)
        {
            ApplicationUser user = await _userManager.Users.FirstOrDefaultAsync(u => u.UserName == userName ||
            u.Email == email && 
            u.UserName != "Admin" && u.Email != null);

            return user == null;
        }

        #endregion

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
