﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;
using Online_wallet_project.Services.FeeServices;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Controllers
{
    [Authorize(Roles = "Manager")]
    public class FeeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly FeeServiceContext _feeServiceContext;
        private readonly FeeStatusReportService _feeStatusReportService;
        private readonly TurnOverReportService _turnOverReportService;
        private readonly FeePartReportService _feePartReportService;
        private readonly FeePaidByUserReportService _feePaidByUserReportService;

        [TempData]
        public string Message { get; set; }

        public FeeController(
            UserManager<ApplicationUser> userManager,
            FeeServiceContext feeServiceContext,
            TurnOverReportService turnOverReportService, 
            FeePartReportService feePartReportService, 
            FeePaidByUserReportService feePaidByUserReportService, 
            FeeStatusReportService feeStatusReportService)
        {
            _userManager = userManager;
            _feeServiceContext = feeServiceContext;
            _turnOverReportService = turnOverReportService;
            _feePartReportService = feePartReportService;
            _feePaidByUserReportService = feePaidByUserReportService;
            _feeStatusReportService = feeStatusReportService;
        }

        public async Task<IActionResult> FeeTypeSelect(int supplierId, string message, bool isEdited)
        {           
            var supplier = await _feeServiceContext.GetSupplier(supplierId);
            if (supplier == null)
            {
                return Content("Поставщик не найден");
            }
            _feeServiceContext.SetFeeReportStrategy(_feeStatusReportService);

            SupplierFeeStatusViewModel model = new SupplierFeeStatusViewModel
            {
                SupplierId = supplier.Id,
                Message = message,
                FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id),
                DateOfStart = DateTime.Today,
                IsEdited = isEdited
            };
            return View(model);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> FeeTypeSelect(SupplierFeeStatusViewModel model, string feeStatus)
        {
            var supplier = await _feeServiceContext.GetSupplier(model.SupplierId);
            _feeServiceContext.SetFeeReportStrategy(_feeStatusReportService);

            model.FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id);

            if (model.DateOfStart < DateTime.Now)
            {
                ModelState.AddModelError("", "\"Дата старта\" должна быть больше текущего дня");
                return View(model);
            }

            var creator = await _userManager.GetUserAsync(User);
            if (feeStatus == SupplierFeeStatusModel.NoCommission)
            {
                FeeReportSaveViewModel saveModel = new FeeReportSaveViewModel
                {
                    FeeStatusReportSaveModel = model,
                    Creator = creator
                };
                saveModel.FeeStatusReportSaveModel.SupplierId = supplier.Id;
                saveModel.FeeStatusReportSaveModel.SelectedFeeStatus = SupplierFeeStatusModel.NoCommission;
                await _feeServiceContext.SaveReport(saveModel);

                return RedirectToAction(nameof(CreateSupplierTurnoverReport), 
                    new { supplierId = model.SupplierId, dateOfStart = model.DateOfStart, isEdited = model.IsEdited });
            }
            else if (feeStatus == SupplierFeeStatusModel.YesCommission)
            {
                FeeReportSaveViewModel saveModel = new FeeReportSaveViewModel
                {
                    FeeStatusReportSaveModel = model,
                    Creator = creator
                };
                saveModel.FeeStatusReportSaveModel.SupplierId = supplier.Id;
                saveModel.FeeStatusReportSaveModel.SelectedFeeStatus = SupplierFeeStatusModel.YesCommission;
                await _feeServiceContext.SaveReport(saveModel);

                return RedirectToAction(nameof(CreateSupplierFeePartReport), 
                    new { supplierId = model.SupplierId, dateOfStart = model.DateOfStart, isEdited = model.IsEdited });
            }

            return PartialView("_StatusMassageForAll", "Error:Произошла ошибка");
        }

        #region CreateSupplierTurnoverReport

        public async Task<IActionResult> CreateSupplierTurnoverReport(int supplierId, DateTime dateOfStart, bool isEdited, int reportId)
        {
            var supplier = await _feeServiceContext.GetSupplier(supplierId);
            if (supplier == null)
            {
                return Content("Поставщик не найден");
            }
            _feeServiceContext.SetFeeReportStrategy(_turnOverReportService);

            SupplierTurnoverViewModel model = new SupplierTurnoverViewModel
            {
                Message = Message,
                SupplierId = supplier.Id,
                DateOfStart = dateOfStart,
                FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id),
            };

            model.FromAmount = await _feeServiceContext.GetFromAmountValue(supplier.Id, model.DateOfStart, isEdited);
            if (isEdited && reportId != 0)
            {
                model.DateOfStart = await _feeServiceContext.GetDateOfStartRewritten(supplier.Id, reportId);
            }
            return View(model);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> CreateSupplierTurnoverReport(SupplierTurnoverViewModel model, string selectedMethod)
        {
            var supplier = await _feeServiceContext.GetSupplier(model.SupplierId);

            if (selectedMethod == null)
            {
                ModelState.AddModelError("", "Не указан способ подсчета");
                return View();
            }
            _feeServiceContext.SetFeeReportStrategy(_turnOverReportService);

            model.FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id);

            if (model.SharedValue.Contains("."))
            {
                model.SharedValue = model.SharedValue.Replace('.', ',');
            }
            if (!decimal.TryParse(model.SharedValue, out var x))
            {
                ModelState.AddModelError("", "Попробуйте еще раз ввести долю");
                return View(model);
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.UpToInfinity)
            {
                model.ToAmount = UInt32.MaxValue;
            }
            if (model.ToAmount <= model.FromAmount)
            {
                ModelState.AddModelError("", $"\"Сумма до\" должна быть больше \"Суммы от\"");
                return View(model);
            }
            if (selectedMethod == CalculatingMethodModel.Percent && decimal.Parse(model.SharedValue) <= 0 || 
                selectedMethod == CalculatingMethodModel.Percent && decimal.Parse(model.SharedValue) > 100)
            {
                ModelState.AddModelError("", "Процент должен быть от 1 до 100");
                return View(model);
            }

            var creator = await _userManager.GetUserAsync(User);
            FeeReportSaveViewModel saveModel = new FeeReportSaveViewModel
            {
                TurnoverReportSaveModel = model,
                Creator = creator
            };
            saveModel.TurnoverReportSaveModel.SupplierId = supplier.Id;
            saveModel.SelectedMethod = selectedMethod;
            await _feeServiceContext.SaveReport(saveModel);

            if (model.ToAmount >= UInt32.MaxValue)
            {
                return RedirectToAction(nameof(FullRangeCoverage), 
                    new { supplierId = supplier.Id, feeStatus = SupplierFeeStatusModel.NoCommission});
            }

            Message = "Новая запись по вознаграждению оператора успешно сохранен";

            return RedirectToAction(nameof(CreateSupplierTurnoverReport), 
                new { supplierId = supplier.Id, dateOfStart = model.DateOfStart});
        }

        #endregion

        #region CreateSupplierFeePartReport

        public async Task<IActionResult> CreateSupplierFeePartReport(int supplierId, DateTime dateOfStart, bool isEdited, int reportId)
        {
            var supplier = await _feeServiceContext.GetSupplier(supplierId);
            if (supplier == null)
            {
                return Content("Поставщик не найден");
            }
            _feeServiceContext.SetFeeReportStrategy(_feePartReportService);

            SupplierFeePartViewModel model = new SupplierFeePartViewModel
            {
                FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id),
                Message = Message,
                SupplierId = supplier.Id,
                DateOfStart = dateOfStart,
                IsEdited = isEdited,
                ReportId = reportId
            };
            if (isEdited && reportId > 0)
            {
                model.DateOfStart = await _feeServiceContext.GetDateOfStartRewritten(supplier.Id, reportId);
            }
            return View(model);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> CreateSupplierFeePartReport(SupplierFeePartViewModel model, string selectedMethod)
        {
            var supplier = await _feeServiceContext.GetSupplier(model.SupplierId);
            _feeServiceContext.SetFeeReportStrategy(_feePartReportService);

            model.FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id);

            if (model.SharedValue.Contains("."))
            {
                model.SharedValue = model.SharedValue.Replace('.', ',');
            }
            if (!decimal.TryParse(model.SharedValue, out var x))
            {
                ModelState.AddModelError("", "Попробуйте еще раз ввести долю");
                return View(model);
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (selectedMethod == null)
            {
                ModelState.AddModelError("", "Не указан способ подсчета");
                return View();
            }
            if (selectedMethod == CalculatingMethodModel.Percent && decimal.Parse(model.SharedValue)  <= 0 || 
                selectedMethod == CalculatingMethodModel.Percent && decimal.Parse(model.SharedValue) > 100)
            {
                ModelState.AddModelError("", "Процент должен быть от 1 до 100");
                return View(model);
            }
            if (await _feePartReportService.GetSupplierFeePartReport(supplier.Id).AnyAsync(
                r => r.SupplierId == supplier.Id && r.DateOfStart == model.DateOfStart && 
                r.CalculatingMethod == selectedMethod && r.SharedValue == decimal.Parse(model.SharedValue)))
            {
                model.Message = "Error:Такая запись с идентичными параметрами уже существует";
                return View(model);
            }

            var creator = await _userManager.GetUserAsync(User);
            FeeReportSaveViewModel saveModel = new FeeReportSaveViewModel
            {
                FeePartReportSaveModel = model,
                Creator = creator
            };
            saveModel.FeePartReportSaveModel.SupplierId = supplier.Id;
            saveModel.SelectedMethod = selectedMethod;
            await _feeServiceContext.SaveReport(saveModel);

            Message = "Новая запись по делению комисии успешно сохранен";

            return RedirectToAction(nameof(CreateSupplierFeePartReport), 
                new { supplierId = supplier.Id, dateOfStart =  model.DateOfStart, isEdited  = model.IsEdited, reportId = model.ReportId});
        }

        #endregion

        #region CreateFeePaidByUser

        public async Task<IActionResult> CreateFeePaidByUser(int supplierId, DateTime dateOfStart, bool isEdited, int reportId)
        {
            var supplier = await _feeServiceContext.GetSupplier(supplierId);
            if (supplier == null)
            {
                return Content("Поставщик не найден");
            }
            _feeServiceContext.SetFeeReportStrategy(_feePaidByUserReportService);

            FeePaidByUserViewModel model = new FeePaidByUserViewModel
            {
                FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplierId),
                Message = Message,
                SupplierId = supplier.Id,
                DateOfStart = dateOfStart,
            };

            model.FromAmount = await _feeServiceContext.GetFromAmountValue(supplierId, model.DateOfStart, isEdited);

            if (isEdited && reportId != 0)
            {
                model.DateOfStart = await _feeServiceContext.GetDateOfStartRewritten(supplierId, reportId);
            }
            return View(model);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> CreateFeePaidByUser(FeePaidByUserViewModel model, string selectedMethod)
        {
            var supplier = await _feeServiceContext.GetSupplier(model.SupplierId);
            _feeServiceContext.SetFeeReportStrategy(_feePaidByUserReportService);

            model.FeeHistoryModel = _feeServiceContext.GetFeeHistoryModel(supplier.Id);

            if (model.FeeValue.Contains("."))
            {
                model.FeeValue = model.FeeValue.Replace('.', ',');
            }
            if (!decimal.TryParse(model.FeeValue, out var x))
            {
                ModelState.AddModelError("", "Попробуйте еще раз ввести комиссию");
                return View(model);
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (selectedMethod == null)
            {
                ModelState.AddModelError("", "Не указан способ подсчета");
                return View();
            }
            if (model.IsUpToMaxSum)
            {
                model.ToAmount = supplier.AmountOfRestriction;
            }
            if (model.ToAmount <= model.FromAmount)
            {
                ModelState.AddModelError("", $"\"Сумма до\" должна быть больше \"Суммы от\"");
                return View(model);
            }
            if (model.ToAmount > supplier.AmountOfRestriction)
            {
                ModelState.AddModelError("", $"\"Сумма до\" не должна быть больше макс. суммы платежа");
                return View(model);
            }
            if (selectedMethod == CalculatingMethodModel.Percent && decimal.Parse(model.FeeValue) <= 0 || 
                selectedMethod == CalculatingMethodModel.Percent && decimal.Parse(model.FeeValue) > 100)
            {
                ModelState.AddModelError("", "Процент должен быть от 1 до 100");
                return View(model);
            }

            var creator = await _userManager.GetUserAsync(User);
            FeeReportSaveViewModel saveModel = new FeeReportSaveViewModel
            {
                FeePaidByUserReportSaveModel = model,
                Creator = creator
            };
            saveModel.FeePaidByUserReportSaveModel.SupplierId = supplier.Id;
            saveModel.SelectedMethod = selectedMethod;
            await _feeServiceContext.SaveReport(saveModel);

            if (model.ToAmount >= supplier.AmountOfRestriction)
            {
                return RedirectToAction(nameof(FullRangeCoverage), 
                    new { supplierId = supplier.Id, feeStatus = SupplierFeeStatusModel.YesCommission });
            }

            Message = "Новая запись по комиссии для клиентов успешно сохранен";

            return RedirectToAction(nameof(CreateFeePaidByUser), 
                new { supplierId = supplier.Id, dateOfStart = model.DateOfStart });
        }

        #endregion


        public async Task<IActionResult> SupplierCommissionInfo(int supplierId)
        {
            var supplier = await _feeServiceContext.GetSupplier(supplierId);
            if (supplier == null)
            {
                return Content("Поставщик не найден");
            }
            _feeServiceContext.SetFeeReportStrategy(_feeStatusReportService);

            SupplierFeeHistoryViewModel historyModel = _feeServiceContext.GetFeeHistoryModel(supplierId);
            historyModel.TurnOverHistory = _turnOverReportService.GetSupplierTurnOverReports(supplierId).ToList();
            historyModel.FeePartHistory = _feePartReportService.GetSupplierFeePartReport(supplierId).ToList();
            historyModel.FeePaidByUserHistory =
                _feePaidByUserReportService.GetSupplierFeePaidByUserReport(supplierId).ToList();
            return View(historyModel);
        }

        public async Task<IActionResult> FullRangeCoverage(int supplierId, string feeStatus)
        {
            var supplier = await _feeServiceContext.GetSupplier(supplierId);
            if (supplier == null)
            {
                return Content("Поставщик не найден");
            }

            SupplierFeeHistoryViewModel historyModel = null;

            if (feeStatus == SupplierFeeStatusModel.NoCommission)
            {
                _feeServiceContext.SetFeeReportStrategy(_turnOverReportService);
                historyModel = _feeServiceContext.GetFeeHistoryModel(supplierId);

                return View(historyModel);
            }
            if (feeStatus == SupplierFeeStatusModel.YesCommission)
            {
                _feeServiceContext.SetFeeReportStrategy(_feePaidByUserReportService);
                historyModel = _feeServiceContext.GetFeeHistoryModel(supplierId);

                return View(historyModel);
            }
            return View();
        }

        public IActionResult DateOfStartSelect(int supplierId, bool isEdited, string methodToRedirect)
        {
            DateOfStartSelectViewModel model = new DateOfStartSelectViewModel
            {
                DateOfStart = DateTime.Today,
                SupplierId = supplierId,
                IsEdited = isEdited,
                MethodToRedirect = methodToRedirect
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult DateOfStartSelect(DateOfStartSelectViewModel model)
        {
            if (model.DateOfStart < DateTime.Now)
            {
                ModelState.AddModelError("", "\"Дата старта\" должна быть больше текущего дня");
                return View(model);
            }
            if (model.MethodToRedirect == "CreateSupplierFeePartReport")
            {
                const int reportId = - 1;
                return RedirectToAction(model.MethodToRedirect,
                    new { supplierId = model.SupplierId, dateOfStart = model.DateOfStart, isEdited = model.IsEdited, reportId });
            }

            return RedirectToAction(model.MethodToRedirect,
                new {supplierId = model.SupplierId, dateOfStart = model.DateOfStart, isEdited = model.IsEdited});
        }
    }
}