﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Models.JsonTransactionModel;
using Online_wallet_project.Models.SupplierViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Controllers
{
    public class TransactionController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public TransactionController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public async Task<IActionResult> Index(int supplierId, string message)
        {
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == supplierId);

            SupplierViewModelForFields model = new SupplierViewModelForFields
            {
                Supplier = supplier,
                FieldTemplates = DeserializeJsonForFieldsTamplate(supplier.FieldData),
                Message = message
            };

            if (!user.EmailConfirmed)
            {
                model.Message = "Error:Подтвердите сначала свою почту";
                return View(model);
            }
            if (!GetCurrentFeeStatusChanges(supplierId).Any())
            {
                model.Message = "Error:Оплата по этому поставщику временно недоступна";
                return View(model);
            }

            string result;
            try
            {
                TransactionExtention transaction = new TransactionExtention(_context, _environment);
                SupplierConfigConnection supplierconfig = await
                    _context.SupplierConfigConnections.FirstOrDefaultAsync(s => s.SupplierId == supplierId);

                Dictionary<string, string> dinamicDictionaries =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(supplierconfig.ConnectionValue);
                result = transaction.Ping(dinamicDictionaries, supplierId);
            }
            catch (Exception)
            {

                return View("BlockingSupplier", model);
            }
            if (supplier.Blocking || result.ToLower() != "ok")
            {
                return View("BlockingSupplier", model);
            }
            return View(model);
        }
        

        [HttpPost]
        public async Task<IActionResult> ForSent(string[] fieldName, string[] value, int supplierId)
        {
            SupplierConfigConnection supplierconfig = await
                _context.SupplierConfigConnections.FirstOrDefaultAsync(s => s.SupplierId == supplierId);
            Dictionary<string, string> dinamicReportRequisite = new Dictionary<string, string>();
            Dictionary<string, string> dinamicDictionaries =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(supplierconfig.ConnectionValue);
            for (int i = 0; i <= fieldName.Length - 1; i++)
            {
                dinamicDictionaries.Add(fieldName[i], value[i]);
                dinamicReportRequisite.Add(fieldName[i], value[i]);
            }
            string json = SerializeToJson(dinamicDictionaries);
            string jsonReportRequisite = SerializeToJson(dinamicReportRequisite);
            
            CheckUserModel checkUserModel;
            try
            {
                TransactionExtention transaction = new TransactionExtention(_context, _environment);
                checkUserModel = transaction.CheckUser(dinamicDictionaries, supplierId);
            }
            catch (Exception)
            {
                const string message = "Error:Сбой в системе";
                return RedirectToAction("Index", new { supplierId, message });
            }
            if (checkUserModel.Status.ToLower() != "ok")
            {
                const string message = "Error:Такого абонента не существует";
                return RedirectToAction("Index", new { supplierId, message });
            }
            
            return RedirectToAction("PayView", new
            {
                name = checkUserModel.UserName,
                lastName = checkUserModel.LastName,
                json,
                supplierId,
                jsonReportRequisite
            });
        }

        [HttpGet]
        public async Task<IActionResult> PayView(string name, string lastName, string json, int supplierId, string jsonReportRequisite)
        {
            CheckUserModel checkUserModel = new CheckUserModel
            {
                UserName = name,
                LastName = lastName
            };
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == supplierId);

            SupplierViewModelForTransactions model = new SupplierViewModelForTransactions
            {
                Json = json,
                SupplierId = supplierId,
                CheckUserModel = checkUserModel,
                Supplier = supplier,
                UserBalance = user.MoneyBalance,
                JsonReportRequisite = jsonReportRequisite
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> PayView(SupplierViewModelForTransactions model)
        {
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == model.SupplierId);

            model.Supplier = supplier;
            model.UserBalance = user.MoneyBalance;

            if (model.SumPay > supplier.AmountOfRestriction)
            {
                model.Message = "Error:Превысили ограничение по оплате";
                return View(model);
            }

            decimal fee = CountFee(model.SupplierId, model.SumPay);
            decimal totalSumOfPay = fee + model.SumPay;

            if (totalSumOfPay < user.MoneyBalance)
            {
                SupplierViewModelForTransactions models = new SupplierViewModelForTransactions
                {
                    SupplierId = model.SupplierId,
                    FeeSum = fee,
                    Json = model.Json,
                    SumPay = model.SumPay,
                    Supplier = supplier,
                    TotalSum = totalSumOfPay,
                    JsonReportRequisite = model.JsonReportRequisite
                };
                return View("ConfirmPayView", models);
            }
            else
            {
                model.Message = "Error:У вас недостаточно средств на счету";
                return View(model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmPayView(SupplierViewModelForTransactions model)
        {
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == model.SupplierId);
            model.Supplier = supplier;

            if (model.TotalSum > user.MoneyBalance)
            {
                ModelState.AddModelError(string.Empty, "У вас недостаточно средств на счету");
                return View(model);
            }

            StatusModel status = await _context.StatusModels.FirstOrDefaultAsync(s => s.StatusName == "Open");

            Transaction transaction = new Transaction
            {
                DateOfCreate = DateTime.Now,
                Requisite = model.Json,
                SumPay = model.SumPay,
                UserId = user.Id,
                SumFee = model.FeeSum,
                SupplierId = model.SupplierId,
                Status = status,
                StatusId = status.Id,
                StatusBody = "NotAccepted",
                GeneratedId = GuidGenerator(),
                ReportRequisite = model.JsonReportRequisite
            };

            try
            {
                TransactionExtention transactionProvider = new TransactionExtention(_context, _environment);

                Dictionary<string, string> dictionary =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(model.Json);
                dictionary.Add("SumPay", model.SumPay.ToString(CultureInfo.InvariantCulture));

                PayModel payModel = transactionProvider.Pay(dictionary, model.SupplierId);
                if (payModel.Status == "Accepted")
                {
                    transaction.TransactionСode = payModel.TransactionId;
                    transaction.StatusBody = payModel.Status;
                    await _context.Transactions.AddAsync(transaction);
                }
                else
                {
                    await _context.Transactions.AddAsync(transaction);

                    status = await _context.StatusModels.FirstOrDefaultAsync(s => s.StatusName == "Error_Net");
                    await _context.Transactions.AddAsync(new Transaction
                    {
                        DateOfCreate = DateTime.Now,
                        Requisite = model.Json,
                        SumPay = model.SumPay,
                        UserId = user.Id,
                        SumFee = model.FeeSum,
                        SupplierId = model.SupplierId,
                        Status = status,
                        StatusId = status.Id,
                        StatusBody = payModel.Status,
                        GeneratedId = transaction.GeneratedId,
                        ReportRequisite = model.JsonReportRequisite
                    });
                }
            }
            catch (Exception ex)
            {
                await _context.Transactions.AddAsync(transaction);
                status = await _context.StatusModels.FirstOrDefaultAsync(s => s.StatusName == "Error_Log");
                await _context.Transactions.AddAsync(new Transaction
                {
                    DateOfCreate = DateTime.Now,
                    Requisite = model.Json,
                    SumPay = model.SumPay,
                    UserId = user.Id,
                    SumFee = model.FeeSum,
                    SupplierId = model.SupplierId,
                    Status = status,
                    StatusId = status.Id,
                    StatusBody = ex.Message,
                    GeneratedId = transaction.GeneratedId,
                    ReportRequisite = model.JsonReportRequisite
                });
            }

            user.MoneyBalance -= model.TotalSum;
            _context.Update(user);
            await _context.SaveChangesAsync();

            model.Message = "Оплата успешна прошла и находится в обработке";
            return View("ConfirmPayResult", model);
        }

        private FieldTemplate[] DeserializeJsonForFieldsTamplate(string json)
        {
            return JsonConvert.DeserializeObject<FieldTemplate[]>(json);
        }

        private string SerializeToJson(Dictionary<string, string> fieldValues)
        {
            return JsonConvert.SerializeObject(fieldValues);
        }

        private decimal CountFee(int supplierId, decimal sum)
        {
            IEnumerable<SupplierFeeStatusChange> feeStatusChanges = GetCurrentFeeStatusChanges(supplierId);

            if (feeStatusChanges.Last().FeeStatus == SupplierFeeStatusModel.YesCommission)
            {
                Fee fee = _context.Fees.Where(f => f.SupplierId == supplierId)
                    .Where(f => f.DateOfStart <= DateTime.Now).Where(f => f.SumMin <= sum && sum <= f.SumMax)
                    .OrderBy(f => f.DateOfStart).ThenBy(f => f.DateOfCreation).ThenBy(r => r.SumMax).Last();

                if (fee.CalculatingMethod == CalculatingMethodModel.FixSom)
                {
                    return fee.FeeValue;
                }
                else
                {
                    const int percent = 100;
                    return sum * fee.FeeValue / percent;
                }
            }
            else
            {
                return 0;
            }
        }

        private IEnumerable<SupplierFeeStatusChange> GetCurrentFeeStatusChanges(int supplierId)
        {
            return _context.SupplierFeeStatusChanges.Where(
                    s => s.SupplierId == supplierId && s.DateOfStart <= DateTime.Now).OrderBy(s => s.DateOfStart)
                .ThenBy(s => s.DateOfCreation);
        }

        public static string GuidGenerator()
        {
            Guid g;
            g = Guid.NewGuid();
            string guid = g.ToString();
            string dateTime = DateTime.Now.ToString();
            string result = guid + dateTime;
            return result;
        }


        public async Task<IActionResult> PayAgain(int id, string message)
        {
            Transaction transaction = await _context.Transactions.FirstOrDefaultAsync(t => t.Id == id);
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == transaction.SupplierId);
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);

            SupplierViewModelForFields model = new SupplierViewModelForFields
            {
                Supplier = supplier,
                FieldTemplates = DeserializeJsonForFieldsTamplate(supplier.FieldData),
                Message = message
            };

            if (!GetCurrentFeeStatusChanges(transaction.SupplierId).Any())
            {
                model.Message = "Error:Оплата по этому поставщику временно недоступна";
                return View("Index");
            }

            string result;

            SupplierConfigConnection supplierconfig = await
                _context.SupplierConfigConnections.FirstOrDefaultAsync(s => s.SupplierId == transaction.SupplierId);

            TransactionExtention transactionExtention = new TransactionExtention(_context, _environment);

            Dictionary<string, string> dinamicDictionaries =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(transaction.Requisite);

            try
            {                             
                result = transactionExtention.Ping(dinamicDictionaries, transaction.SupplierId);
            }
            catch (Exception)
            {
                return View("BlockingSupplier", model);
            }
            if (supplier.Blocking || result.ToLower() != "ok")
            {
                return View("BlockingSupplier", model);
            }

            CheckUserModel checkUserModel;

            try
            {
                checkUserModel = transactionExtention.CheckUser(dinamicDictionaries, transaction.SupplierId);
            }
            catch (Exception)
            {
                const string messageError = "Error:Сбой в системе";
                return RedirectToAction("Index", new { supplierId = transaction.SupplierId, message = messageError });
            }
            if (checkUserModel.Status.ToLower() != "ok")
            {
                const string messageError = "Error:Такого абонента не существует";
                return RedirectToAction("Index", new { supplierId = transaction.SupplierId, message = messageError });
            }

            string json = SerializeToJson(dinamicDictionaries);

            SupplierViewModelForTransactions modelForTransactions = new SupplierViewModelForTransactions
            {
                Json = json,
                SupplierId = transaction.SupplierId,
                CheckUserModel = checkUserModel,
                Supplier = supplier,
                UserBalance = user.MoneyBalance
            };
            return View(modelForTransactions);

        }

        [HttpPost]
        public async Task<IActionResult> PayAgain(SupplierViewModelForTransactions modelForTransactions)
        {
            ApplicationUser user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            SupplierModel supplier = await _context.Suppliers.FirstOrDefaultAsync(s => s.Id == modelForTransactions.SupplierId);

            modelForTransactions.Supplier = supplier;
            modelForTransactions.UserBalance = user.MoneyBalance;

            if (modelForTransactions.SumPay > supplier.AmountOfRestriction)
            {
                modelForTransactions.Message = "Error:Превысили ограничение по оплате";
                return View(modelForTransactions);
            }

            decimal fee = CountFee(modelForTransactions.SupplierId, modelForTransactions.SumPay);
            decimal totalSumOfPay = fee + modelForTransactions.SumPay;

            if (totalSumOfPay < user.MoneyBalance)
            {
                return RedirectToAction("ConfirmPayView",
                    new
                    {
                        sumPay = modelForTransactions.SumPay,
                        fee,
                        supplierId = modelForTransactions.SupplierId,
                        json = modelForTransactions.Json
                    });
            }
            else
            {
                modelForTransactions.Message = "Error:У вас недостаточно средств на счету";
                return View(modelForTransactions);
            }
        }

    }
}