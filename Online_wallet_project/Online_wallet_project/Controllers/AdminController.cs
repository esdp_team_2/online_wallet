using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Models.AccountViewModels;
using Online_wallet_project.Models.AdminViewModel;
using Online_wallet_project.Models.ViewModels;
using Microsoft.Extensions.Options;
using Online_wallet_project.Services;

namespace Online_wallet_project.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("[controller]/[action]")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;
        private readonly PagingService _pagingService;
        private readonly UserLockOutSettings _userLockOutSettings;
        private readonly SessionOptionSettings _sessionOptionSettings;
        private CustomValodatorPassword _customValodatorPassword;
        
        public AdminController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            ILogger<AccountController> logger,
            PagingService pagingService,
            IOptions<UserLockOutSettings> userLockOutSettings,
            IOptions<SessionOptionSettings> sessionOptionSettings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _logger = logger;
            _pagingService = pagingService;
            _userLockOutSettings = userLockOutSettings.Value;
            _sessionOptionSettings = sessionOptionSettings.Value;
            _customValodatorPassword = new CustomValodatorPassword(_context, _context.ValidationSettingses.FirstOrDefault().LengthPasswordInt);

        }

        public IActionResult Index(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Search(string name,
            DateTime? dateTimeFrom, DateTime? dateTimeTo,
            int page = 1, SortState sortOrder = SortState.DateDesc)
        {
            IQueryable<ApplicationUser> users = await ForValidationUsersAndAdmin.GetUsersOnlyByRole("User", _userManager, "BlockedUser");
            if (users != null)
            {
                users = ForFiltrAndSort.FilterForUser(name, dateTimeTo, dateTimeFrom, users);
                users = ForFiltrAndSort.SortUser(sortOrder, users);

                // пэйджинг
                PagedObject<ApplicationUser> pagedObject = _pagingService.DoPage(users, page);

                AdminPagingViewModel viewModel = new AdminPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    SortViewModel = new SortViewModel(sortOrder),
                    FilterViewModel = new FilterViewModel(name, dateTimeFrom, dateTimeTo,null, null),
                    Users = pagedObject.Objects
                };

                return View(viewModel);
            }

            return View();
        }

        public async Task<IActionResult> BlockingOfUser(string Id, string userName)
        {
            await UniversalMethodForLockingAndUnlocking(Id, "BlockedUser", true);

            return RedirectToAction("Search", "Admin", new { keyWord = userName });
        }

        public async Task<IActionResult> UnBlockingOfUser(string Id, string userName)
        {
            await UniversalMethodForLockingAndUnlocking(Id, "User",false);

            return RedirectToAction("Search", "Admin", new { keyWord = userName});
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            // Очистите существующий внешний файл cookie, чтобы обеспечить чистый процесс входа в систему
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(AdminLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await ForValidationUsersAndAdmin.GetUserRolesOnRole(_userManager, "Admin", model.Login))
                {
                    var result =
                        await _signInManager.PasswordSignInAsync(model.Login, model.Password, false,
                            lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("Admin logged in.");
                        return RedirectToAction("Index", "Admin");
                    }

                    ModelState.AddModelError(string.Empty, "Неправильный логин и/или пароль.");
                    return View(model);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Неправильный логин и/или пароль");
                    return View(model);
                }
            }
            return View(model);
        }

        // Вывел логику блокировки и разблокировки потомучто у тебя она дублируется
        private async Task UniversalMethodForLockingAndUnlocking(string id, string nameRole, bool forIsLocked)
        {
            ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.Id == id);
            user.IsLocked = forIsLocked;
            IdentityRole role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == nameRole);
            IdentityUserRole<string> userRole = await _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == user.Id);
            _context.UserRoles.Remove(userRole);
            _context.UserRoles.Add(new IdentityUserRole<string>
            {
                UserId = user.Id,
                RoleId = role.Id
            });
            _context.SaveChanges();
        }

        public IActionResult CreateManager() => View();

        [HttpPost]
        public async Task<IActionResult> CreateManager(CreateManagerVewModel model)
        {
            var resultValidatorPassword = await _customValodatorPassword.ValidateAsync(model.Password);
            if (resultValidatorPassword.Succeeded)
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.Login,
                        DateOfCreation = DateTime.Now
                    };

                    if (await ValidateForLoginAndEmail(user))
                    {
                        return View(model);
                    }

                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        //Присвоивание пользователю при регистрации роли "Manager"
                        IdentityRole role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == "Manager");
                        await _context.UserRoles.AddAsync(new IdentityUserRole<string>
                        {
                            RoleId = role.Id,
                            UserId = user.Id
                        });
                        await _context.SaveChangesAsync();

                        _logger.LogInformation("Admin created a new Manager with password.");
                        return RedirectToAction("Index", "Admin",
                            new {message = $"Менеджер {model.Login} с паролем {model.Password} создан!"});
                    }
                    
                }
            }
            else
            {
                foreach (var error in resultValidatorPassword.Errors)
                {
                    ModelState.AddModelError(String.Empty, error.Description);
                }

                return View(model);
            }
            ModelState.AddModelError(String.Empty, "Логин не должен содержать пробелов");
            return View(model);
        }


        [NonAction]
        private async Task<bool> ValidateForLoginAndEmail(ApplicationUser user)
        {
            bool result = false;

            if (!await TrueOrFalseUniqeLoginNameOrGmail(user.UserName))
            {
                ModelState.AddModelError(string.Empty, "Такой пользователь с таким \"Логином\" существует");
                result = true;
            }

            return result;
        }

        [NonAction]
        private async Task<bool> TrueOrFalseUniqeLoginNameOrGmail(string userName)
        {
            ApplicationUser user = await _userManager.Users.FirstOrDefaultAsync(u => u.UserName == userName &&
                                                                                 u.UserName != "Admin");
            return user == null;
        }

        [HttpGet]
        public IActionResult ValidationSettingsPassword(int page = 1)
        {
            ValidationSettings validationSettings = _context.ValidationSettingses.FirstOrDefault();
            IQueryable<BlackPassword> queryableBlackPasswords = _context.BlackPasswords;

            PagedObject<BlackPassword> pagedObject = _pagingService.DoPage(queryableBlackPasswords, page);

            ValidationSettingViewModel model = new ValidationSettingViewModel
            {
                PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                ValidationSettings = validationSettings,
                BlackPasswords = pagedObject.Objects
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ValidationSettingsPassword(ValidationSettingViewModel model, int Id)
        {
            if (ModelState.IsValid)
            {
                var validationSetting =
                    _context.ValidationSettingses.FirstOrDefault(v => v.Id == Id);

                validationSetting.BlackPassword = model.ValidationSettings.BlackPassword;
                validationSetting.LengthPassword = model.ValidationSettings.LengthPassword;
                validationSetting.LengthPasswordInt = model.ValidationSettings.LengthPasswordInt;
                validationSetting.PatternInt = model.ValidationSettings.PatternInt;
                validationSetting.PatternStringDown = model.ValidationSettings.PatternStringDown;
                validationSetting.PatternUPString = model.ValidationSettings.PatternUPString;
                validationSetting.PatternSpecialСharacters = model.ValidationSettings.PatternSpecialСharacters;

                _context.ValidationSettingses.Update(validationSetting);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("ValidationSettingsPassword", "Admin");
        }

        [HttpPost]
        public async Task<IActionResult> AddBlackPasswordTable(ValidationSettingViewModel model, string text_for_pars)
        {
            if (text_for_pars != null)
            {
                string[] blackPasswordaArray = text_for_pars.Split(new char[]{'\r', ' ', '\n',}).Where(n => !string.IsNullOrEmpty(n)).ToArray();
                List<BlackPassword> blackPassword = _context.BlackPasswords.ToList();
                for (int i = 0; i < blackPasswordaArray.Length; i++)
                {
                    blackPassword.Add(new BlackPassword() { UnsuitablePassword = blackPasswordaArray[i] });
                }

                _context.BlackPasswords.UpdateRange(blackPassword);
                await _context.SaveChangesAsync();
            }
            
            return RedirectToAction("ValidationSettingsPassword", "Admin");
        }

        [HttpPost]
        public IActionResult DeleteBlackPassword(int delete)
        {
            var deleteBlackPassword = _context.BlackPasswords.FirstOrDefault(d => d.Id == delete);
            _context.Remove(deleteBlackPassword);
            _context.SaveChanges();

            return RedirectToAction("ValidationSettingsPassword", "Admin");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult EditSettings()
        {
            SettingsViewModel model = new SettingsViewModel
            {
                UserLockOutSettings = _userLockOutSettings,
                SessionOptionSettings = _sessionOptionSettings
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult EditUserLockoutSettings(SettingsViewModel model)
        {
            if (model.UserLockOutSettings.UserLockoutEnabledByDefault != null)
            {
                _userLockOutSettings.UserLockoutEnabledByDefault = model.UserLockOutSettings.UserLockoutEnabledByDefault;
            }
            if (model.UserLockOutSettings.DefaultLockoutTimeSpan != null)
            {
                _userLockOutSettings.DefaultLockoutTimeSpan = model.UserLockOutSettings.DefaultLockoutTimeSpan;
            }
            if (model.UserLockOutSettings.MaxFailedAccessAttempts != null)
            {
                _userLockOutSettings.MaxFailedAccessAttempts = model.UserLockOutSettings.MaxFailedAccessAttempts;
            }

            _userLockOutSettings.UpdateJsonSettings();

            return RedirectToAction("EditSettings");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult EditSessionSettings(SettingsViewModel model)
        {
            if (model.SessionOptionSettings.IdleTimeout != null)
            {
                _sessionOptionSettings.IdleTimeout = model.SessionOptionSettings.IdleTimeout;
            }
            _sessionOptionSettings.UpdateJsonSettings();

            return RedirectToAction("EditSettings");
        }

        [HttpGet]
        public IActionResult ShedullerSettings()
        {
            ShedullerSetting shedullerSettings = _context.ShedullerSettings.FirstOrDefault();
            ShedullerSetingViewModel model = new ShedullerSetingViewModel
            {
                Id = shedullerSettings.Id,
                PeriodTime = shedullerSettings.PeriodTime,
                StartTime = shedullerSettings.StartTime
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult ShedullerSettings(ShedullerSetingViewModel model)
        {
            ShedullerSetting shedullerSettings = _context.ShedullerSettings.FirstOrDefault(s => s.Id == model.Id);
            shedullerSettings.StartTime = model.StartTime;
            shedullerSettings.PeriodTime = model.PeriodTime;

            _context.ShedullerSettings.Update(shedullerSettings);
            _context.SaveChanges();
            return RedirectToAction("Index", "Admin");
        }
    }
}