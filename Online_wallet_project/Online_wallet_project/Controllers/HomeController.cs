﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Online_wallet_project.Data;
using Online_wallet_project.Models;

namespace Online_wallet_project.Controllers
{
    [Authorize (Roles = "User")]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index(string message)
        {
            IEnumerable<GroupModel> model = _context.GroupModels;
            ViewBag.Message = message;
            return View(model);
        }

        public IActionResult ViewSuppliers(int groupId)
        {
            IEnumerable<SupplierModel> suppliers = _context.Suppliers.Where(s => s.GroupModelId == groupId);
            return View(suppliers);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
