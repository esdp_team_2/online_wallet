﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Models.AdminViewModel;
using Online_wallet_project.Models.ManagerViewModels;
using Online_wallet_project.Models.SupplierUserViewModel;
using Online_wallet_project.Models.ViewModels;
using Online_wallet_project.Services;

namespace Online_wallet_project.Controllers
{
    [Authorize(Roles = "Supplier")]
    [Route("SupplierUser/[action]")]
    public class SupplierUserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly PagingService _pagingService;

        public SupplierUserController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            PagingService pagingService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _pagingService = pagingService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            // Очистите существующий внешний файл cookie, чтобы обеспечить чистый процесс входа в систему
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(ManagerLoginViewModel userModel)
        {
            if (ModelState.IsValid)
            {
                if (await ForValidationUsersAndAdmin.GetUserRolesOnRole(_userManager, "Supplier", userModel.Login))
                {
                    var result =
                        await _signInManager.PasswordSignInAsync(userModel.Login, userModel.Password, false,
                            lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "SupplierUser");
                    }

                    ModelState.AddModelError(string.Empty, "Неправильный логин и/или пароль.");
                    return View(userModel);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Неправильный логин и/или пароль");
                    return View(userModel);
                }
            }
            return View(userModel);
        }

        public IActionResult ShowAllTransaction(DateTime? dateTimeFrom, DateTime? dateTimeTo,
            decimal? sumFrom, decimal? sumTo, int page = 1)
        {
            ApplicationUser userSup = _context.Users.FirstOrDefault(s => s.UserName == User.Identity.Name);
            IQueryable<Transaction> transactions = _context.Transactions
                .Where(t => t.SupplierId == userSup.SupplierId)
                .Include(t => t.Status)
                .Where(t => t.Status.StatusName == "Pay" || t.Status.StatusName == "Returne");

            if (transactions.Any() && dateTimeFrom != null || dateTimeTo != null || sumFrom != null || sumTo != null)
            {
                transactions =
                    ForFiltrAndSort.FilterForTransaction(dateTimeTo, dateTimeFrom, sumFrom, sumTo, transactions);

                // пэйджинг
                PagedObject<Transaction> pagedObject = _pagingService.DoPage(transactions, page);

                SupplierPagingViewModel viewModel = new SupplierPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, sumFrom, sumTo),
                    Transactions = pagedObject.Objects
                };

                return View(viewModel);
            }
            else
            {
                transactions.Where(t => t.DateOfCreate.Month == DateTime.Now.Month);
                PagedObject<Transaction> pagedObject = _pagingService.DoPage(transactions, page);

                SupplierPagingViewModel viewModel = new SupplierPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, sumFrom, sumTo),
                    Transactions = pagedObject.Objects
                };
                return View(viewModel);
            }
        }

        public IActionResult ShowProfit(DateTime? dateTimeTo, DateTime? dateTimeFrom, int page = 1)
        {
            ApplicationUser userSup = _context.Users.FirstOrDefault(s => s.UserName == User.Identity.Name);

            SupplierFeeStatusChange status =
                _context.SupplierFeeStatusChanges.FirstOrDefault(s => s.SupplierId == userSup.SupplierId);

            string queryString = $"Select Sum(SumPay) as 'SumPay', SUM(SumFee) as 'SumFee'," +
                                 $" Format(Min(DateOfCreate),'MM/yyyy','ru-RU') as 'DateOfTime'" +
                                 $" from Transactions where StatusId = 2" +
                                 $" group by DATEPART(yyyy, DateOfCreate), DATEPART(mm, DateOfCreate)";


            List<SupplierUserGroupingTransaction> model = ForSqlQuery.SqlQueryMethod(queryString);
            List<SupplierUserGroupingTransaction> newModel = new List<SupplierUserGroupingTransaction>();

            if (status.FeeStatus == SupplierFeeStatusModel.NoCommission)
            {
                IEnumerable<SuppliersTurnoverReport> report =
                    _context.SuppliersTurnoverReports.Where(t => t.SupplierId == userSup.SupplierId);
               

                foreach (SupplierUserGroupingTransaction groupingTransaction in model)
                {
                    decimal profit = 0;

                    foreach (SuppliersTurnoverReport r in report)
                    {
                        if (r.CalculatingMethod == CalculatingMethodModel.Percent)
                        {
                            profit += groupingTransaction.AllSum * r.SharedValue / 100;
                        }
                        else
                        {
                            if (r.SumMin <= groupingTransaction.AllSum && r.SumMax >= groupingTransaction.AllSum)
                            {
                                profit += r.SharedValue;
                            }
                        }
                    }

                    newModel.Add(new SupplierUserGroupingTransaction
                    {
                        AllFee = groupingTransaction.AllFee,
                        AllSum = groupingTransaction.AllSum,
                        Date = groupingTransaction.Date,
                        Profit = profit
                    });
                }

                if (dateTimeTo != null || dateTimeFrom != null)
                {
                    newModel = ForFiltrAndSort.FilterForProfitTransaction(dateTimeTo,dateTimeFrom, newModel);
                }

                PagedObject<SupplierUserGroupingTransaction> pagedObject = _pagingService.DoPage(newModel.AsQueryable(), page);

                SupplierUserShowProfitPagingViewModel viewModel = new SupplierUserShowProfitPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null,dateTimeFrom,dateTimeTo,null,null),
                    SupplierUserGroupingTransactions = pagedObject.Objects.OrderByDescending(t => t.Date.Year).ToList()
                };

                return View(viewModel);
            }
            else
            {
                IEnumerable<FeePartReport> report =
                   _context.FeePartReports.Where(t => t.SupplierId == userSup.SupplierId);


                foreach (SupplierUserGroupingTransaction groupingTransaction in model)
                {
                    decimal profit = 0;

                    foreach (FeePartReport r in report)
                    {
                        if (r.CalculatingMethod == CalculatingMethodModel.Percent)
                        {
                            profit += groupingTransaction.AllFee * r.SharedValue / 100;
                        }
                    }

                    newModel.Add(new SupplierUserGroupingTransaction
                    {
                        AllFee = groupingTransaction.AllSum,
                        AllSum = groupingTransaction.AllFee,
                        Date = groupingTransaction.Date,
                        Profit = profit
                    });
                }

                if (dateTimeTo != null || dateTimeFrom != null)
                {
                    newModel = ForFiltrAndSort.FilterForProfitTransaction(dateTimeTo, dateTimeFrom, newModel);
                }

                PagedObject<SupplierUserGroupingTransaction> pagedObject = _pagingService.DoPage(newModel.AsQueryable(), page);

                SupplierUserShowProfitPagingViewModel viewModel = new SupplierUserShowProfitPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new FilterViewModel(null, dateTimeFrom, dateTimeTo, null, null),
                    SupplierUserGroupingTransactions = pagedObject.Objects.OrderByDescending(t => t.Date.Year).ToList()
                };

                return View(viewModel);
            }
        }
    }
}