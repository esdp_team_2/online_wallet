﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class Add_Tables_Fee_And_FeePartReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeePartReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Creator = table.Column<int>(nullable: false),
                    DateOfCreation = table.Column<DateTime>(nullable: false),
                    SupplierId = table.Column<int>(nullable: false),
                    SupplierModelId = table.Column<int>(nullable: true),
                    SupplierPercent = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeePartReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeePartReports_Suppliers_SupplierModelId",
                        column: x => x.SupplierModelId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Fees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Closed = table.Column<bool>(nullable: false),
                    Creator = table.Column<int>(nullable: false),
                    DateOfCreation = table.Column<DateTime>(nullable: false),
                    DateOfStart = table.Column<DateTime>(nullable: false),
                    FeeType = table.Column<string>(nullable: true),
                    FeeValue = table.Column<decimal>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    SumMax = table.Column<decimal>(nullable: false),
                    SumMin = table.Column<decimal>(nullable: false),
                    SupplierId = table.Column<int>(nullable: false),
                    SupplierModelId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fees_Suppliers_SupplierModelId",
                        column: x => x.SupplierModelId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FeePartReports_SupplierModelId",
                table: "FeePartReports",
                column: "SupplierModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Fees_SupplierModelId",
                table: "Fees",
                column: "SupplierModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeePartReports");

            migrationBuilder.DropTable(
                name: "Fees");
        }
    }
}
