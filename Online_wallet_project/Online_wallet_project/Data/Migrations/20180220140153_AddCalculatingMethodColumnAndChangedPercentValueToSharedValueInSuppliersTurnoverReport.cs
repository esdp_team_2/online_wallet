﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class AddCalculatingMethodColumnAndChangedPercentValueToSharedValueInSuppliersTurnoverReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ValuePercent",
                table: "SuppliersTurnoverReports",
                newName: "SharedValue");

            migrationBuilder.AddColumn<string>(
                name: "CalculatingMethod",
                table: "SuppliersTurnoverReports",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CalculatingMethod",
                table: "SuppliersTurnoverReports");

            migrationBuilder.RenameColumn(
                name: "SharedValue",
                table: "SuppliersTurnoverReports",
                newName: "ValuePercent");
        }
    }
}
