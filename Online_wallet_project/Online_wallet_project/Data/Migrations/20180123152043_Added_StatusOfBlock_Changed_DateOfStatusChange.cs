﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class Added_StatusOfBlock_Changed_DateOfStatusChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateOfBlock",
                table: "BlockedEverUsers",
                newName: "DateOfStatusChange");

            migrationBuilder.AddColumn<int>(
                name: "StatusOfBlock",
                table: "BlockedEverUsers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusOfBlock",
                table: "BlockedEverUsers");

            migrationBuilder.RenameColumn(
                name: "DateOfStatusChange",
                table: "BlockedEverUsers",
                newName: "DateOfBlock");
        }
    }
}
