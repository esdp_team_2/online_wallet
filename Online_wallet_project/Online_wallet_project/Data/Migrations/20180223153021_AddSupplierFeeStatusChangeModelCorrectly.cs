﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class AddSupplierFeeStatusChangeModelCorrectly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SupplierFeeStatusChanges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatorId = table.Column<string>(nullable: true),
                    DateOfCreation = table.Column<DateTime>(nullable: false),
                    DateOfStart = table.Column<DateTime>(nullable: false),
                    FeeStatus = table.Column<string>(nullable: true),
                    SupplierId = table.Column<int>(nullable: false),
                    SupplierModelId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierFeeStatusChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupplierFeeStatusChanges_AspNetUsers_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupplierFeeStatusChanges_Suppliers_SupplierModelId",
                        column: x => x.SupplierModelId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SupplierFeeStatusChanges_CreatorId",
                table: "SupplierFeeStatusChanges",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierFeeStatusChanges_SupplierModelId",
                table: "SupplierFeeStatusChanges",
                column: "SupplierModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SupplierFeeStatusChanges");
        }
    }
}
