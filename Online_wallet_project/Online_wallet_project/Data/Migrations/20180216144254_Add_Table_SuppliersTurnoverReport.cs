﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class Add_Table_SuppliersTurnoverReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SuppliersTurnoverReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Creator = table.Column<int>(nullable: false),
                    DateOfCreation = table.Column<DateTime>(nullable: false),
                    SumMax = table.Column<decimal>(nullable: false),
                    SumMin = table.Column<decimal>(nullable: false),
                    SupplierId = table.Column<int>(nullable: false),
                    SupplierModelId = table.Column<int>(nullable: true),
                    ValuePercent = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersTurnoverReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuppliersTurnoverReports_Suppliers_SupplierModelId",
                        column: x => x.SupplierModelId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersTurnoverReports_SupplierModelId",
                table: "SuppliersTurnoverReports",
                column: "SupplierModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SuppliersTurnoverReports");
        }
    }
}
