﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class Addition_TransactionCodeAndStatusBody_Deleting_DateOfComletion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOfCompletion",
                table: "Transactions");

            migrationBuilder.AddColumn<string>(
                name: "StatusBody",
                table: "Transactions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransactionСode",
                table: "Transactions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusBody",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "TransactionСode",
                table: "Transactions");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfCompletion",
                table: "Transactions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
