﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class add_table_validation_setting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ValidationSettingses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlackPassword = table.Column<bool>(nullable: false),
                    LengthPassword = table.Column<bool>(nullable: false),
                    LengthPasswordInt = table.Column<int>(nullable: false),
                    PatternInt = table.Column<bool>(nullable: false),
                    PatternSpecialСharacters = table.Column<bool>(nullable: false),
                    PatternStringDown = table.Column<bool>(nullable: false),
                    PatternUPString = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidationSettingses", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ValidationSettingses");
        }
    }
}
