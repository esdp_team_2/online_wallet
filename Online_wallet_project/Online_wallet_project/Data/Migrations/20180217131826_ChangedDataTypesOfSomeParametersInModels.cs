﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class ChangedDataTypesOfSomeParametersInModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Creator",
                table: "SuppliersTurnoverReports");

            migrationBuilder.DropColumn(
                name: "Creator",
                table: "Fees");

            migrationBuilder.DropColumn(
                name: "Creator",
                table: "FeePartReports");

            migrationBuilder.AddColumn<string>(
                name: "CreatorId",
                table: "SuppliersTurnoverReports",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AmountOfRestriction",
                table: "Suppliers",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<string>(
                name: "CreatorId",
                table: "Fees",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatorId",
                table: "FeePartReports",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ManagerId",
                table: "BlockedSuppliers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersTurnoverReports_CreatorId",
                table: "SuppliersTurnoverReports",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Fees_CreatorId",
                table: "Fees",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_FeePartReports_CreatorId",
                table: "FeePartReports",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_BlockedSuppliers_ManagerId",
                table: "BlockedSuppliers",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_BlockedSuppliers_SupplierId",
                table: "BlockedSuppliers",
                column: "SupplierId");

            migrationBuilder.AddForeignKey(
                name: "FK_BlockedSuppliers_AspNetUsers_ManagerId",
                table: "BlockedSuppliers",
                column: "ManagerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BlockedSuppliers_Suppliers_SupplierId",
                table: "BlockedSuppliers",
                column: "SupplierId",
                principalTable: "Suppliers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FeePartReports_AspNetUsers_CreatorId",
                table: "FeePartReports",
                column: "CreatorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Fees_AspNetUsers_CreatorId",
                table: "Fees",
                column: "CreatorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SuppliersTurnoverReports_AspNetUsers_CreatorId",
                table: "SuppliersTurnoverReports",
                column: "CreatorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlockedSuppliers_AspNetUsers_ManagerId",
                table: "BlockedSuppliers");

            migrationBuilder.DropForeignKey(
                name: "FK_BlockedSuppliers_Suppliers_SupplierId",
                table: "BlockedSuppliers");

            migrationBuilder.DropForeignKey(
                name: "FK_FeePartReports_AspNetUsers_CreatorId",
                table: "FeePartReports");

            migrationBuilder.DropForeignKey(
                name: "FK_Fees_AspNetUsers_CreatorId",
                table: "Fees");

            migrationBuilder.DropForeignKey(
                name: "FK_SuppliersTurnoverReports_AspNetUsers_CreatorId",
                table: "SuppliersTurnoverReports");

            migrationBuilder.DropIndex(
                name: "IX_SuppliersTurnoverReports_CreatorId",
                table: "SuppliersTurnoverReports");

            migrationBuilder.DropIndex(
                name: "IX_Fees_CreatorId",
                table: "Fees");

            migrationBuilder.DropIndex(
                name: "IX_FeePartReports_CreatorId",
                table: "FeePartReports");

            migrationBuilder.DropIndex(
                name: "IX_BlockedSuppliers_ManagerId",
                table: "BlockedSuppliers");

            migrationBuilder.DropIndex(
                name: "IX_BlockedSuppliers_SupplierId",
                table: "BlockedSuppliers");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "SuppliersTurnoverReports");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "Fees");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "FeePartReports");

            migrationBuilder.AddColumn<int>(
                name: "Creator",
                table: "SuppliersTurnoverReports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<double>(
                name: "AmountOfRestriction",
                table: "Suppliers",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<int>(
                name: "Creator",
                table: "Fees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Creator",
                table: "FeePartReports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "ManagerId",
                table: "BlockedSuppliers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
