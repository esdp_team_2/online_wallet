﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class Added_Field_ConnectionValue_SupplierConfigConnection_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConnectionValue",
                table: "SupplierConfigConnections",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConnectionValue",
                table: "SupplierConfigConnections");
        }
    }
}
