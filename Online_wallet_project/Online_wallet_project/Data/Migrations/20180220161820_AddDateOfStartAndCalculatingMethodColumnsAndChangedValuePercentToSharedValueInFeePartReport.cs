﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Online_wallet_project.Data.Migrations
{
    public partial class AddDateOfStartAndCalculatingMethodColumnsAndChangedValuePercentToSharedValueInFeePartReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SupplierPercent",
                table: "FeePartReports",
                newName: "SharedValue");

            migrationBuilder.AddColumn<string>(
                name: "CalculatingMethod",
                table: "FeePartReports",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfStart",
                table: "FeePartReports",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CalculatingMethod",
                table: "FeePartReports");

            migrationBuilder.DropColumn(
                name: "DateOfStart",
                table: "FeePartReports");

            migrationBuilder.RenameColumn(
                name: "SharedValue",
                table: "FeePartReports",
                newName: "SupplierPercent");
        }
    }
}
