﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Models;

namespace Online_wallet_project.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<BlockedEverUser> BlockedEverUsers { get; set; }
        public DbSet<BlackPassword> BlackPasswords { get; set; }
        public DbSet<ValidationSettings> ValidationSettingses { get; set; }
        public DbSet<GroupModel> GroupModels { get; set; }
        public DbSet<SupplierModel> Suppliers { get; set; }
        public DbSet<BlockedSupplier> BlockedSuppliers { get; set; }
        public DbSet<UserEmailChange> UserEmailChanges { get; set; }
        public DbSet<UserPasswordChange> UserPasswordChanges { get; set; }
        public DbSet<FeePartReport> FeePartReports { get; set; }
        public DbSet<Fee> Fees { get; set; }
        public DbSet<SuppliersTurnoverReport> SuppliersTurnoverReports { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<StatusModel> StatusModels { get; set; }
        public DbSet<SupplierFeeStatusChange> SupplierFeeStatusChanges { get; set; }
        public DbSet<SupplierConfigConnection> SupplierConfigConnections { get; set; }
        public DbSet<ShedullerSetting> ShedullerSettings { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
