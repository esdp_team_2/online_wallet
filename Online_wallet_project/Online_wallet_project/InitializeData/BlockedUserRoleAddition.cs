﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Online_wallet_project.InitializeData
{
    [NonController]
    public class BlockedUserRoleAddition
    {
        public static void Initialize(ApplicationDbContext context, UserManager<ApplicationUser> _userManager)
        {
            if (!context.ValidationSettingses.Any())
            {
                context.ValidationSettingses.Add(new ValidationSettings
                {
                    BlackPassword = false,
                    LengthPasswordInt = 10,
                    LengthPassword = true,
                    PatternInt = false,
                    PatternStringDown = false,
                    PatternUPString = false,
                    PatternSpecialСharacters = false
                });

                context.SaveChanges();
            }

            if (!context.ShedullerSettings.Any())
            {
                ShedullerSetting shedullerSetting = new ShedullerSetting
                {
                    StartTime = 1,
                    PeriodTime = 5
                };

                context.ShedullerSettings.Add(shedullerSetting);
                context.SaveChanges();
            }

            if (!context.Roles.Any())
            {
                List<IdentityRole> roles = new List<IdentityRole>
                {
                    new IdentityRole
                    {
                        Name = "Admin"
                    },
                    new IdentityRole
                    {
                        Name = "User"
                    },
                    new IdentityRole
                    {
                        Name = "BlockedUser"
                    },
                    new IdentityRole
                    {
                        Name = "Manager"
                    },
                    new IdentityRole
                    {
                        Name = "Supplier"
                    }

                };
                context.Roles.AddRange(roles);
                context.SaveChanges();
            }

            if (SearchAdminIsUsers(context))
            {
                var result = _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "Admin",
                    DateOfCreation = DateTime.Now
                }, "12qw!@QW");
                if (result.Result.Succeeded)
                {
                    ApplicationUser user = context.Users.FirstOrDefault(u => u.UserName == "Admin");
                    IdentityRole role = context.Roles.FirstOrDefault(r => r.Name == "Admin");
                    context.UserRoles.Add(new IdentityUserRole<string>
                    {
                        RoleId = role.Id,
                        UserId = user.Id
                    });
                    context.SaveChanges();
                }
            }

            if (!context.GroupModels.Any())
            {
                context.Add(new GroupModel
                {
                    GroupName = "Общая группа",
                    GroupLogo = $"images/DefaultLogos/DefaultGroupLogo.png"
                });
                context.SaveChanges();
            }

            if (!context.StatusModels.Any())
            {
                List<StatusModel> status = new List<StatusModel>
                {
                    new StatusModel
                    {
                        StatusName = "Open"
                    },
                    new StatusModel
                    {
                        StatusName = "Pay"
                    },
                    new StatusModel
                    {
                        StatusName = "Error_Log"
                    },
                    new StatusModel
                    {
                        StatusName = "Error_Net"
                    },
                    new StatusModel
                    {
                        StatusName = "Returne"
                    },
                    new StatusModel
                    {
                        StatusName = "Close"
                    }
                };

                context.StatusModels.AddRange(status);
                context.SaveChanges();
            }
        }

        private static bool SearchAdminIsUsers(ApplicationDbContext context)
        {
            return context.Users.FirstOrDefault(u => u.UserName == "Admin") == null;
        }
    }
}
