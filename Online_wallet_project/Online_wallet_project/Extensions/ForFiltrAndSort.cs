﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierUserViewModel;

namespace Online_wallet_project.Extensions
{
    public static class ForFiltrAndSort
    {
        public static IQueryable<ApplicationUser> FilterForUser(string name, DateTime? dateTo, DateTime? dateFrom, IQueryable<ApplicationUser> users)
        {
            if (!string.IsNullOrEmpty(name))
            {
                users = users.Where(u => u.UserName.Contains(name));
            }
            if (dateFrom != null)
            {
                users = users.Where(u => u.DateOfCreation >= dateFrom);
            }
            if (dateTo != null)
            {
                users = users.Where(u => u.DateOfCreation <= dateTo);
            }

            return users;
        }

        public static IQueryable<Transaction> FilterForTransaction(DateTime? dateTo, DateTime? dateFrom,
            decimal? sumFrom, decimal? sumTo, IQueryable<Transaction> transactions)
        {
            if (dateTo != null)
            {
                transactions = transactions.Where(t => t.DateOfCreate <= dateTo);
            }
            if (dateFrom != null)
            {
                transactions = transactions.Where(t => t.DateOfCreate >= dateFrom);
            }
            if (sumTo != null)
            {
                transactions = transactions.Where(t => t.SumPay <= sumTo);
            }
            if (sumFrom != null)
            {
                transactions = transactions.Where(t => t.SumPay >= sumFrom);
            }

            return transactions;
        }

        public static List<SupplierUserGroupingTransaction> FilterForProfitTransaction(DateTime? dateTo, DateTime? dateFrom,
            IEnumerable<SupplierUserGroupingTransaction> model)
        {

            if (dateTo != null)
            {
                model = model.Where(t => t.Date <= dateTo);
            }
            if (dateFrom != null)
            {
                model = model.Where(t => t.Date >= dateFrom);
            }

            return model.ToList();
        }

        public static IQueryable<ApplicationUser> SortUser(SortState sortState, IQueryable<ApplicationUser> users)
        {
            switch (sortState)
            {
                case SortState.NameDesc:
                    users = users.OrderByDescending(u => u.UserName);
                    break;
                case SortState.DateDesc:
                    users = users.OrderBy(u => u.DateOfCreation);
                    break;
                case SortState.DateAsc:
                    users = users.OrderByDescending(u => u.DateOfCreation);
                    break;
                default:
                    users = users.OrderBy(u => u.UserName);
                    break;
            }

            return users;
        }
    }
}
