﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Online_wallet_project.Data;
using Online_wallet_project.Models;


namespace Online_wallet_project.Extensions
{
    public class CustomValodatorPassword
    {
        private readonly ApplicationDbContext _context;

        public int RequiredLength { get; set; }

        public CustomValodatorPassword(ApplicationDbContext context, int requiredLength)
        {
            _context = context;
            RequiredLength = requiredLength;
        }
        
        public  Task<IdentityResult> ValidateAsync(string password)
        {
            List<IdentityError> errors = new List<IdentityError>();

            ValidationSettings validationSettings = _context.ValidationSettingses.FirstOrDefault();

            char[] passwordCharArray = password.ToCharArray();

            if (validationSettings.LengthPassword)
            {
                if (String.IsNullOrEmpty(password) || password.Length < RequiredLength)
                {
                    errors.Add(new IdentityError
                    {
                        Description = String.Format("Минимальная длина пароля равна {0}", RequiredLength)
                    });
                }
            }

            if (validationSettings.BlackPassword)
            {
                if (null != _context.BlackPasswords.FirstOrDefault(p => p.UnsuitablePassword == password))
                {
                    errors.Add(new IdentityError
                    {
                        Description = "Пароль является не надежным и находится в списке простых паролей"
                    });
                }
            }

            if (validationSettings.PatternInt)
            {
                UniversalMethodPatternValidation(errors, passwordCharArray, "Пароль должен содержать хоть одну цифру", "^[0-9]+$");
            }

            if (validationSettings.PatternUPString)
            {
                UniversalMethodPatternValidation(errors, passwordCharArray, "Пароль должен содержать хоть одну заглавную букву", "^[A-ZА-Я]+$");
            }

            if (validationSettings.PatternStringDown)
            {
                UniversalMethodPatternValidation(errors, passwordCharArray, "Пароль должен содержать хоть одну прописную букву", "^[a-zа-я]+$");
            }

            if (validationSettings.PatternSpecialСharacters)
            {
                UniversalMethodPatternValidation(errors, passwordCharArray, "Пароль должен содержать хоть один спец символ", @"[!@#$%^&*\(\)_\+\-\={}<>,\.\|""'~`:;\\?\/\[\]]");                
            }

            return Task.FromResult(errors.Count == 0 ? IdentityResult.Success : IdentityResult.Failed(errors.ToArray()));
        }

        private void UniversalMethodPatternValidation(List<IdentityError> errors, char[] passwordCharArray, string errorMessage, string pattern)
        {
            bool resultError = true;

            foreach (var u in passwordCharArray)
            {
                if (Regex.IsMatch(u.ToString(), pattern))
                {
                    resultError = false;
                    break;
                }
            }
            if (resultError)
            {
                errors.Add(new IdentityError
                {
                    Description = errorMessage
                });
            }
        }
    }
}
