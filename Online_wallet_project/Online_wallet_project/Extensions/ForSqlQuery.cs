﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Online_wallet_project.Models.SupplierUserViewModel;

namespace Online_wallet_project.Extensions
{
    public class ForSqlQuery
    {
        public static List<SupplierUserGroupingTransaction> SqlQueryMethod(string sqlQuery)
        {
            List<SupplierUserGroupingTransaction> groupingTransactions = new List<SupplierUserGroupingTransaction>();

            dynamic blogs = JObject.Parse(System.IO.File.ReadAllText("appsettings.json"));
            string connectionString = blogs.ConnectionStrings.DefaultConnection;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sqlQuery, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        groupingTransactions.Add(new SupplierUserGroupingTransaction
                        {
                            AllFee = (decimal)reader["SumFee"],
                            AllSum = (decimal)reader["SumPay"],
                            Date = DateTime.Parse("1." + (string)reader["DateOfTime"])
                        });
                    }
                }
                finally
                {
                    reader.Close();
                }

                return groupingTransactions;
            }
        }
    }
}
