using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Controllers;

namespace Microsoft.AspNetCore.Mvc
{
    public static class UrlHelperExtensions
    {
        public static string EmailConfirmationLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ConfirmEmail),
                controller: "Account",
                values: new { userId, code },
                protocol: scheme);
        }

        public static string ResetPasswordCallbackLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ResetPassword),
                controller: "Account",
                values: new { userId, code },
                protocol: scheme);
        }

        public static string ChangePasswordCallbackLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(ManageController.ChangePassword),
                controller: "Manage",
                values: new { userId, code},
                protocol: scheme);
        }

        public static string ChangeEmailConfirmationLink(this IUrlHelper urlHelper, string userId, string code, string newEmail, string scheme)
        {
            return urlHelper.Action(
                action: nameof(ManageController.ConfirmEmailChange),
                controller: "Manage",
                values: new { userId, code, newEmail },
                protocol: scheme);
        }

        public static string CheckUserProfileChangeLink(this IUrlHelper urlHelper, string message,string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.Login),
                controller: "Account",
                values: message,
                protocol: scheme);
        }
    }
}
