using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Online_wallet_project.Services;

namespace Online_wallet_project.Services
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailAsync(email, "����������� ��� ����� ����������� �����",
                $"����������� ���� ����� ����� ��� ������: <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#4184f3;border-radius:2px;min-width:90px\" class=\"\"><tbody><tr style=\"height:3px\"></tr><tr><td style=\"padding-left:8px;padding-right:8px;text-align:center\" id=\"mail\" class=\"\"><a href='{HtmlEncoder.Default.Encode(link)}' style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:11px\" target=\"_blank\" id=\"z\" class=\"\">c����� ��� ��������</a></td></tr><tr style=\"height:3px\"></tr></tbody></table>");
        }

        public static Task SendEmailToSetPassword(this IEmailSender emailSender, string email, string subject, string link)
        {
            return emailSender.SendEmailAsync(email, subject,
                $"��������� �� ������ ��� �������������� ������:<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#4184f3;border-radius:2px;min-width:90px\" class=\"\"><tbody><tr style=\"height:3px\"></tr><tr><td style=\"padding-left:8px;padding-right:8px;text-align:center\" class=\"\"><a href='{HtmlEncoder.Default.Encode(link)}' style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:11px\" target=\"_blank\" class=\"\">c����� ��� ��������</a></td></tr><tr style=\"height:3px\"></tr></tbody></table>");
        }

        public static Task SendEmailChangeConfirmationAsync(this IEmailSender emailSender, string email, string subject, string link)
        {
            return emailSender.SendEmailAsync(email, subject,
                $"����������� ����� ����� ����� ��� ������: <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#4184f3;border-radius:2px;min-width:90px\" class=\"\"><tbody><tr style=\"height:3px\"></tr><tr><td style=\"padding-left:8px;padding-right:8px;text-align:center\" class=\"\"><a href='{HtmlEncoder.Default.Encode(link)}' style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:11px\" target=\"_blank\" class=\"\">c����� ��� ��������</a></td></tr><tr style=\"height:3px\"></tr></tbody></table>");
        }

        public static Task SendEmailUserProfileChangeAsync(this IEmailSender emailSender, string email, string subject, string link)
        {
            return emailSender.SendEmailAsync(email, subject,
                $"��������� ���� ������� ����� ��� ������: <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#4184f3;border-radius:2px;min-width:90px\" class=\"\"><tbody><tr style=\"height:3px\"></tr><tr><td style=\"padding-left:8px;padding-right:8px;text-align:center\" class=\"\"><a href='{HtmlEncoder.Default.Encode(link)}' style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:11px\" target=\"_blank\" class=\"\">c����� ��� ��������</a></td></tr><tr style=\"height:3px\"></tr></tbody></table>");
        }
    }
}
