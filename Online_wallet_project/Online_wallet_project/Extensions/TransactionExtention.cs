﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.JsonTransactionModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Online_wallet_project.Extensions
{
    public class TransactionExtention
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public TransactionExtention(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public string Ping(Dictionary<string, string> dictionary, int idSupplier)
        {
            SupplierConfigConnection sConfigConnection =
                _context.SupplierConfigConnections.FirstOrDefault(s => s.SupplierId == idSupplier);
            Dictionary<string, object> methodObjects =  ConnectDll(sConfigConnection.DllPath, "Ping");

            MethodInfo method =  (MethodInfo)methodObjects["method"];
            object o = methodObjects["object"];

            object[] paramsMethod = new object[1];

            paramsMethod[0] = dictionary;

            object json = method.Invoke(o, paramsMethod);

            PingModel model = JsonConvert.DeserializeObject<PingModel>(json.ToString());

            return model.Status;
        }

        public CheckUserModel CheckUser(Dictionary<string, string> dictionary, int idSupplier)
        {
            SupplierConfigConnection sConfigConnection =
                _context.SupplierConfigConnections.FirstOrDefault(s => s.SupplierId == idSupplier);
            Dictionary<string, object> methodObjects = ConnectDll(sConfigConnection.DllPath, "CheckUser");

            MethodInfo method = (MethodInfo)methodObjects["method"];
            object o = methodObjects["object"];

            object[] paramsMethod = new object[1];

            paramsMethod[0] = dictionary;

            object json = method.Invoke(o, paramsMethod);

            CheckUserModel model = JsonConvert.DeserializeObject<CheckUserModel>(json.ToString());

            return model;
        }

        public PayModel Pay(Dictionary<string, string> dictionary, int idSupplier)
        {
            SupplierConfigConnection sConfigConnection =
                _context.SupplierConfigConnections.FirstOrDefault(s => s.SupplierId == idSupplier);
            Dictionary<string, object> methodObjects = ConnectDll(sConfigConnection.DllPath, "Pay");

            MethodInfo method = (MethodInfo)methodObjects["method"];
            object o = methodObjects["object"];

            object[] paramsMethod = new object[1];

            paramsMethod[0] = dictionary;

            object json = method.Invoke(o, paramsMethod);

            PayModel model = JsonConvert.DeserializeObject<PayModel>(json.ToString());

            return model;
        }

        public string CheckPay(Dictionary<string, string> dictionary, int idSupplier)
        {
            SupplierConfigConnection sConfigConnection =
                _context.SupplierConfigConnections.FirstOrDefault(s => s.SupplierId == idSupplier);
            Dictionary<string, object> methodObjects = ConnectDll(sConfigConnection.DllPath, "CheckPay");

            MethodInfo method = (MethodInfo)methodObjects["method"];
            object o = methodObjects["object"];

            object[] paramsMethod = new object[1];

            paramsMethod[0] = dictionary;

            object json = method.Invoke(o, paramsMethod);

            CheckPayModel model = JsonConvert.DeserializeObject<CheckPayModel>(json.ToString());

            return model.Status;
        }

        private Dictionary<string, object> ConnectDll(string pathToDll, string nameMethod)
        {
            Dictionary<string,object> dictionary = new Dictionary<string, object>();

            Assembly assembly = Assembly.LoadFile(Path.Combine(_environment.WebRootPath,pathToDll));

            Object o = assembly.CreateInstance("TransactionClass");

            Type t = assembly.GetType("TransactionClass");

            MethodInfo method = t.GetMethod(nameMethod);

            dictionary.Add("method",method);
            dictionary.Add("object",o);

            return dictionary;
        }
    }
}