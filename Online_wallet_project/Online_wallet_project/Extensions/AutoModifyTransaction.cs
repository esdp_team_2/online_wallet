﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Online_wallet_project.Models.JsonTransactionModel;

namespace Online_wallet_project.Extensions
{
    public class AutoModifyTransaction
    {
        public IHostingEnvironment _environment;
        private readonly ILogger _logger;

        public AutoModifyTransaction(IHostingEnvironment environment, ILogger<AutoModifyTransaction> loger)
        {
            _environment = environment;
            _logger = loger;
        }

        public AutoModifyTransaction()
        {

        }

        public void Start(object state)
        {
            dynamic blogs = JObject.Parse(File.ReadAllText("appsettings.json"));
             var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer((string)blogs.ConnectionStrings.DefaultConnection);
            using (var _context = new ApplicationDbContext(optionsBuilder.Options))
            {
                
                TransactionExtention transactionExtention = new TransactionExtention(_context, _environment);
                
                //список уже закрытых транзакций
                IQueryable<Transaction> transactionsClose = _context.Transactions.Where(t => t.StatusId == 6);
                
                //список открытых транзакций
                IQueryable<Transaction> transactionsOpen = _context.Transactions.Where(t => t.StatusId == 1);

                //сортирую открытые транзакции по закрытым с уникальным GeneratedId для них
                foreach (Transaction transaction in transactionsClose)
                {
                    for (int i = 0; i < transactionsClose.Count(); i++)
                    {
                        transactionsOpen =  transactionsOpen.Where(t => t.GeneratedId != transaction.GeneratedId && t.StatusId == 1);
                    }
                }
                
                //создаю список транзакций с ошибочным статусом сети
                IQueryable<Transaction> transactionsError = _context.Transactions.Where(t => t.StatusId == 4);
                
                //Сортирую список ошибочных транзакций которые уже завершены чтобы не проводить их заново
                foreach (Transaction transaction in transactionsClose)
                {
                    for (int i = 0; i < transactionsError.Count(); i++)
                    {
                        transactionsError = transactionsError.Where(t =>
                            t.GeneratedId != transaction.GeneratedId && t.StatusId == 4);
                    }
                }

                //пытаюсь провести повторно оплату транзакций с ошибочным статусом сети
                foreach (var transaction in transactionsError)
                {
                       TransactionPay(transaction, _context);
                }
                
                //все открытые транзакции проверяю на статус оплаты, в зависимости от возвратного статуса делаю оплату или же возврат
                foreach (var transaction in transactionsOpen)
                {
                    Dictionary<string, string> dictionary = new Dictionary<string, string>();
                    dictionary.Add("transactionId", transaction.TransactionСode);
                    Dictionary<string, string> dictionaryForPing = JsonConvert.DeserializeObject<Dictionary<
                        string, string>>(transaction.Requisite);
                    string result = transactionExtention.Ping(dictionaryForPing, transaction.SupplierId);
                    if (result == "ok")
                    {
                        dynamic status = transactionExtention.CheckPay(dictionary, transaction.SupplierId);
                        string[] arrayStatus = new string[3];
                        switch (status)
                        {
                            case "Success":
                                arrayStatus[0] = status;
                                arrayStatus[1] = "Pay";
                                //arrayStatus[2] = "Close";
                                TransactionUpdate(transaction, _context, arrayStatus);
                                break;
                            case "Waiting":
                                break;
                            case "Error":
                                arrayStatus[0] = status;
                                arrayStatus[1] = "Returne";
                                //arrayStatus[2] = "Close";
                                TransactionUpdate(transaction, _context, arrayStatus);
                                break;
                        }
                    }
                }
                
                //создаю список транзакиций которые успешно оплачены и не завершены
                IQueryable<Transaction> transactionsPay = _context.Transactions.Where(t => t.StatusId == 2);

                foreach (var transaction in transactionsClose)
                {
                    transactionsPay =
                        transactionsPay.Where(t => t.GeneratedId != transaction.GeneratedId && t.StatusId == 2);
                }

                //закрываю транзакции со статусом оплачено
                foreach (var transaction in transactionsPay)
                {
                    TransactionClose(transaction, _context);
                }

                //создаю возвратные транзакции из ошибочных
                foreach (Transaction transaction in transactionsError)
                {
                    TransactionReturn(transaction, _context);
                }
                
                //создаю список возвратных транзакци 
                IQueryable<Transaction> transactionsReturn =
                    _context.Transactions.Where(t => t.StatusId == 5);
                
                //создаю список из возвратных транзакций
                foreach (Transaction transaction in transactionsClose)
                {
                    for (int i = 0; i < transactionsReturn.Count(); i++)
                    {
                        transactionsReturn = transactionsReturn.Where(t =>
                            t.GeneratedId != transaction.GeneratedId && t.StatusId == 5);
                    }
                }

                //хожу по возвратным и закрываю их
                foreach (var transaction in transactionsReturn)
                {
                    TransactionClose(transaction, _context);
                }
            }
        }

        private void TransactionUpdate(Transaction transaction, ApplicationDbContext context, string[] status)
        {
            StatusModel statusOpen = context.StatusModels.FirstOrDefault(s => s.StatusName == status[1]);

            Transaction newTransactionOpen = new Transaction
            {
                DateOfCreate = DateTime.Now,
                Requisite = transaction.Requisite,
                SumPay = transaction.SumPay,
                UserId = transaction.UserId,
                SumFee = transaction.SumFee,
                SupplierId = transaction.SupplierId,
                Status = statusOpen,
                StatusId = statusOpen.Id,
                StatusBody = status[0],
                GeneratedId = transaction.GeneratedId,
                TransactionСode = transaction.TransactionСode,
                ReportRequisite = transaction.ReportRequisite
            };
            context.Transactions.Add(newTransactionOpen);
            context.SaveChanges();
            _logger.LogInformation("Update transaction {0}", transaction.Id);
        }

        private void TransactionPay(Transaction transaction, ApplicationDbContext context)
        {
            TransactionExtention transactionExtention = new TransactionExtention(context, _environment);
            Dictionary<string, string> dictionary =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(transaction.Requisite);
            dictionary.Add("SumPay", transaction.SumPay.ToString());
            string result = transactionExtention.Ping(dictionary, transaction.SupplierId);
            if (result == "ok")
            {
               
                PayModel payModel = transactionExtention.Pay(dictionary, transaction.SupplierId);

                if (payModel.Status == "Accepted")
                {
                    StatusModel status = context.StatusModels.FirstOrDefault(s => s.StatusName == "Open");

                    transaction = new Transaction
                    {
                        DateOfCreate = DateTime.Now,
                        Requisite = transaction.Requisite,
                        SumPay = transaction.SumPay,
                        UserId = transaction.UserId,
                        SumFee = transaction.SumFee,
                        SupplierId = transaction.SupplierId,
                        Status = status,
                        StatusId = status.Id,
                        StatusBody = payModel.Status,
                        GeneratedId = transaction.GeneratedId,
                        TransactionСode = payModel.TransactionId,
                        ReportRequisite = transaction.ReportRequisite
                    };
                    
                    context.Transactions.Update(transaction);
                    context.SaveChanges();
                    _logger.LogInformation($"Create transaction pay {transaction.Id}");
                }
            }
        }

        private void TransactionReturn(Transaction transaction, ApplicationDbContext context)
        {
            ApplicationUser user = context.Users.FirstOrDefault(u => u.Id == transaction.UserId);
            if (transaction.StatusId == 4)
            {
                string[] arrayStatus = new string[3];
                arrayStatus[0] = transaction.StatusBody;
                arrayStatus[1] = "Returne";
                TransactionUpdate(transaction,context,arrayStatus);
            
                user.MoneyBalance = user.MoneyBalance + transaction.SumFee + transaction.SumPay;
                context.Users.Update(user);
                context.SaveChanges();
                _logger.LogInformation("Create transaction Retern {transactionid}", transaction.Id);
            }
        }

        private void TransactionClose(Transaction transaction, ApplicationDbContext context)
        {
            StatusModel statusClose = context.StatusModels.FirstOrDefault(s => s.StatusName == "Close");

            Transaction newTransactionClose = new Transaction
            {
                DateOfCreate = DateTime.Now,
                Requisite = transaction.Requisite,
                SumPay = transaction.SumPay,
                UserId = transaction.UserId,
                SumFee = transaction.SumFee,
                SupplierId = transaction.SupplierId,
                Status = statusClose,
                StatusId = statusClose.Id,
                StatusBody = transaction.StatusBody,
                GeneratedId = transaction.GeneratedId,
                TransactionСode = transaction.TransactionСode,
                ReportRequisite = transaction.ReportRequisite
            };

            context.Transactions.Add(newTransactionClose);
            context.SaveChanges();

            _logger.LogInformation("Create transaction Close {transactionid}", transaction.Id);
        }
    }
}
