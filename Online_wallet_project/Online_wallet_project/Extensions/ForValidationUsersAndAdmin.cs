﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Online_wallet_project.Controllers;
using Online_wallet_project.Models;

namespace Online_wallet_project.Extensions
{
    public static class ForValidationUsersAndAdmin
    {
        public static async Task<bool> GetUserRolesOnRole(UserManager<ApplicationUser> _userManager, string role, string modelLogin)
        {
            if (await GetUserRole(modelLogin, null, _userManager) == role) return true;
            else return false;
        }

        public static async Task<string> GetUserRole(string login, string id, UserManager<ApplicationUser> _userManager)
        {
            ApplicationUser user = !string.IsNullOrEmpty(login)
                ? await _userManager.FindByNameAsync(login)
                : await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                var result = await _userManager.GetRolesAsync(user);
                return result[0];
            }
            else
            {
                return "нет прав!";
            }
        }

        public static async Task<IQueryable<ApplicationUser>> GetUsersOnlyByRole(string role, UserManager<ApplicationUser> _userManager, string role2 = null)
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            foreach (ApplicationUser user in _userManager.Users)
            {
                if (await GetUserRole(user.UserName, null, _userManager) == role || await GetUserRole(user.UserName, null, _userManager) == role2)
                {
                    users.Add(user);
                }
            }
            return users.Count == 0 ? null : users.ToArray().AsQueryable();
        }

        public static async Task CheckBlockedOrNot(UserManager<ApplicationUser> _userManager,
            SignInManager<ApplicationUser> _signInManager,
            string name, string role)
        {
            if (await GetUserRole(name, null, _userManager) != role)
            {
                await _signInManager.SignOutAsync();
            }
        }
    }
}
