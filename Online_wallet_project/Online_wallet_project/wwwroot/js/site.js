﻿
function RenderInfo(text, classes) {
    return `<div class="${classes}" style="color:#a94442;">${text}</div>`;
}

function ClickRemoveElement() {
    $('.danger').remove();
}

function ValidateSingleInput(oInput, _validFileExtensions, size) {
    if (oInput.type === "file") {
        let sFileName = oInput.files[0];

        if (_validFileExtensions !== sFileName.type) {
            oInput.value = "";
            $(oInput).after(RenderInfo('Недопустимое расширение', 'danger'));
        }
        if (sFileName.size > size *1024) {
            oInput.value = "";
            $(oInput).after(RenderInfo(`Размер не должен превышать ${size} кб`, 'danger'));
        }
    }
}

function formClick(nform, groupName) {

    for (var i = 0; i < nform.length; i++) {
        if (nform[i].tagName === "FORM") {
            if (nform[i].className === groupName) {
                nform[i].submit();
                break;
            }
        } else {
            nform.submit();
            break;
        }
    }
}
