﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.InitializeData;
using Online_wallet_project.Models;
using System;
using System.Linq;
using System.Threading;

namespace Online_wallet_project
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = BuildWebHost(args);

            using (var scope = webHost.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    var userManager = services.GetService<UserManager<ApplicationUser>>();
                    var environment = services.GetService<IHostingEnvironment>();
                    var loger = services.GetService<ILogger<AutoModifyTransaction>>();
                    BlockedUserRoleAddition.Initialize(context, userManager);
                    int startTime = context.ShedullerSettings.FirstOrDefault().StartTime;
                    int periodTime = context.ShedullerSettings.FirstOrDefault().PeriodTime;
                    AutoModifyTransaction autoModifyTransaction = new AutoModifyTransaction(environment, loger);
                    TimerCallback timeCB = autoModifyTransaction.Start;
                    Timer time = new Timer(timeCB, null, (startTime * 10) * 1000, (periodTime * 60) * 1000);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "Error while initializing model data");
                }
            }

            webHost.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
        
    }
}
