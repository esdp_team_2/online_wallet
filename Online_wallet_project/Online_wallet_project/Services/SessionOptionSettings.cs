﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Online_wallet_project.Services
{
    public class SessionOptionSettings
    {
        public double? IdleTimeout { get; set; }

        public void UpdateJsonSettings()
        {
            System.IO.File.WriteAllText("sessionoptionssettings.json",
                JsonConvert.SerializeObject(this, Formatting.Indented));
        }
    }
}
