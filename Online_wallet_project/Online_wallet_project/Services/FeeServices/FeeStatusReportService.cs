﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;

namespace Online_wallet_project.Services.FeeServices
{
    public class FeeStatusReportService : CommonFeeReportService, IFeeService
    {
        public FeeStatusReportService(ApplicationDbContext context) : base(context)
        {
        }

        public SupplierFeeHistoryViewModel GetFeeHistoryModel(int supplierId)
        {
            SupplierFeeHistoryViewModel historyModel = GetOnlyFeeStatusHistoryModel(supplierId).Result;
            return historyModel;
        }

        public async Task SaveReport(FeeReportSaveViewModel model)
        {
            SupplierFeeStatusChange feeStatusChange = new SupplierFeeStatusChange
            {
                SupplierId = model.FeeStatusReportSaveModel.SupplierId,
                CreatorId = model.Creator.Id,
                DateOfStart = model.FeeStatusReportSaveModel.DateOfStart,
                DateOfCreation = DateTime.Now,
                FeeStatus = model.FeeStatusReportSaveModel.SelectedFeeStatus
            };
            await Context.SupplierFeeStatusChanges.AddAsync(feeStatusChange);
            await Context.SaveChangesAsync();
        }

        public Task<DateTime> GetDateOfStartRewritten(int supplierId, int reportId) => throw new NotImplementedException();

        public Task<decimal> GetFromAmountValue(int supplierId, DateTime dateOfStart, bool isEdited) => throw new NotImplementedException();
    }
}
