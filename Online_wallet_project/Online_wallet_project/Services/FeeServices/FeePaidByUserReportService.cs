﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;

namespace Online_wallet_project.Services.FeeServices
{
    public class FeePaidByUserReportService : CommonFeeReportService, IFeeService
    {
        public FeePaidByUserReportService(ApplicationDbContext context) : base(context)
        {
        }

        public SupplierFeeHistoryViewModel GetFeeHistoryModel(int supplierId)
        {
            SupplierFeeHistoryViewModel historyModel = GetOnlyFeeStatusHistoryModel(supplierId).Result;

            IQueryable<Fee> feePaidByUserReports = GetSupplierFeePaidByUserReport(supplierId);

            IQueryable<FeePartReport> feePartReports = GetSupplierFeePartReport(supplierId);

            historyModel.FeePaidByUserHistory = feePaidByUserReports.ToList();
            historyModel.FeePartHistory = feePartReports.ToList();
            return historyModel;
        }

        public async Task SaveReport(FeeReportSaveViewModel model)
        {
            Fee report = new Fee
            {
                CalculatingMethod = model.SelectedMethod,
                FeeValue = decimal.Parse(model.FeePaidByUserReportSaveModel.FeeValue),
                DateOfCreation = DateTime.Now,
                DateOfStart = model.FeePaidByUserReportSaveModel.DateOfStart,
                SupplierId = model.FeePaidByUserReportSaveModel.SupplierId,
                CreatorId = model.Creator.Id,
                SumMin = model.FeePaidByUserReportSaveModel.FromAmount,
                SumMax = model.FeePaidByUserReportSaveModel.ToAmount,
            };

            await Context.Fees.AddAsync(report);
            await Context.SaveChangesAsync();
        }

        public async Task<DateTime> GetDateOfStartRewritten(int supplierId, int reportId)
        {
            IQueryable<Fee> feePaidByUserReport = GetSupplierFeePaidByUserReport(supplierId);
            Fee report = await feePaidByUserReport.FirstOrDefaultAsync(r => r.Id == reportId);
            return report.DateOfStart;
        }

        public async Task<decimal> GetFromAmountValue(int supplierId, DateTime dateOfStart, bool isEdited)
        {
            IQueryable<Fee> feePaidByUserReport =  GetSupplierFeePaidByUserReport(supplierId);
            decimal fromAmount = 0;
            const decimal increment = 0.01M;

            if (await feePaidByUserReport.AnyAsync(r => r.DateOfStart == dateOfStart) && !isEdited)
            {
                fromAmount = feePaidByUserReport.Last(r => r.DateOfStart == dateOfStart).SumMax + increment;
                return fromAmount;
            }
            return fromAmount;
        }

        public IQueryable<Fee> GetSupplierFeePaidByUserReport(int supplierId)
        {
            IQueryable<Fee> feePaidByUserReports =
                Context.Fees.Where(r => r.SupplierId == supplierId)
                    .OrderBy(r => r.DateOfStart).ThenBy(r => r.DateOfCreation).ThenBy(r => r.SumMax);

            return feePaidByUserReports;
        }

        private IQueryable<FeePartReport> GetSupplierFeePartReport(int supplierId)
        {
            IQueryable<FeePartReport> feePartReports =
                Context.FeePartReports.Where(r => r.SupplierId == supplierId)
                    .OrderBy(r => r.DateOfStart).ThenBy(r => r.DateOfCreation);

            return feePartReports;
        }
    }
}
