﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;

namespace Online_wallet_project.Services.FeeServices
{
    public interface IFeeService
    {
        SupplierFeeHistoryViewModel GetFeeHistoryModel(int supplierId);
        Task SaveReport(FeeReportSaveViewModel model);
        Task<DateTime> GetDateOfStartRewritten(int supplierId, int reportId);
        Task<decimal> GetFromAmountValue(int supplierId, DateTime dateOfStart, bool isEdited);
    }
}
