﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;

namespace Online_wallet_project.Services.FeeServices
{
    public sealed class TurnOverReportService : CommonFeeReportService, IFeeService
    {
        public TurnOverReportService(ApplicationDbContext context) : base(context)
        {
        }

        public SupplierFeeHistoryViewModel GetFeeHistoryModel(int supplierId)
        {
            SupplierFeeHistoryViewModel historyModel = GetOnlyFeeStatusHistoryModel(supplierId).Result;

            IQueryable<SuppliersTurnoverReport> turnOverReports = GetSupplierTurnOverReports(supplierId);

            historyModel.TurnOverHistory = turnOverReports.ToList();

            return historyModel;
        }

        public async Task SaveReport(FeeReportSaveViewModel model)
        {
            SuppliersTurnoverReport report = new SuppliersTurnoverReport
            {
                SumMin = model.TurnoverReportSaveModel.FromAmount,
                SumMax = model.TurnoverReportSaveModel.ToAmount,
                CalculatingMethod = model.SelectedMethod,
                SharedValue = decimal.Parse(model.TurnoverReportSaveModel.SharedValue),
                DateOfCreation = DateTime.Now,
                DateOfStart = model.TurnoverReportSaveModel.DateOfStart,
                SupplierId = model.TurnoverReportSaveModel.SupplierId,
                CreatorId = model.Creator.Id,
            };
            await Context.SuppliersTurnoverReports.AddAsync(report);
            await Context.SaveChangesAsync();
        }

        public async Task<DateTime> GetDateOfStartRewritten(int supplierId, int reportId)
        {
            IQueryable<SuppliersTurnoverReport> turnOverReports = GetSupplierTurnOverReports(supplierId);
            SuppliersTurnoverReport report = await turnOverReports.FirstOrDefaultAsync(r => r.Id == reportId);
            return report.DateOfStart;
        }

        public async Task<decimal> GetFromAmountValue(int supplierId, DateTime dateOfStart, bool isEdited)
        {
            IQueryable<SuppliersTurnoverReport> turnOverReports = GetSupplierTurnOverReports(supplierId);
            decimal fromAmount = 0;
            const decimal increment = 0.01M;

            if (await turnOverReports.AnyAsync(r => r.DateOfStart == dateOfStart) && !isEdited)
            {
                SuppliersTurnoverReport report = await turnOverReports.LastAsync(r => r.DateOfStart == dateOfStart);
                fromAmount = report.SumMax + increment;
                return fromAmount;
            }
            return fromAmount;
        }

        public IQueryable<SuppliersTurnoverReport> GetSupplierTurnOverReports(int supplierId)
        {
            IQueryable<SuppliersTurnoverReport> turnOverReports = Context.SuppliersTurnoverReports
                .Where(r => r.SupplierId == supplierId).OrderBy(r => r.DateOfStart).ThenBy(r => r.DateOfCreation)
                .ThenBy(r => r.SumMax);

            return turnOverReports;
        }
    }
}
