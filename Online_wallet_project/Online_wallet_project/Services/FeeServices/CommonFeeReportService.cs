﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;

namespace Online_wallet_project.Services.FeeServices
{
    public class CommonFeeReportService
    {
        protected readonly ApplicationDbContext Context;

        public CommonFeeReportService(ApplicationDbContext context)
        {
            Context = context;
        }

        protected async Task<SupplierFeeHistoryViewModel> GetOnlyFeeStatusHistoryModel(int supplierId)
        {
            SupplierFeeHistoryViewModel historyModel = new SupplierFeeHistoryViewModel
            {
                Supplier = await GetSupplier(supplierId),
            };

            if (await Context.SupplierFeeStatusChanges.AnyAsync(fs => fs.SupplierId == supplierId))
            {
                IQueryable<SupplierFeeStatusChange> feeStatusChanges = Context.SupplierFeeStatusChanges
                    .Where(fs => fs.SupplierId == supplierId)
                    .OrderBy(fs => fs.DateOfStart)
                    .ThenBy(fs => fs.DateOfCreation);

                historyModel.FeeStatusChanges = feeStatusChanges.ToList();

                feeStatusChanges = feeStatusChanges.Where(fs => fs.DateOfStart <= DateTime.Now.Date);

                if (await feeStatusChanges.CountAsync() != 0)
                {
                    historyModel.CurrentFeeStatusId = feeStatusChanges.Last().Id;
                    historyModel.FeeStatusName = feeStatusChanges.Last().FeeStatus;
                }
            }
            return historyModel;
        }

        public async Task<SupplierModel> GetSupplier(int supplierId)
        {
            SupplierModel supplier = await Context.Suppliers.FirstOrDefaultAsync(s => s.Id == supplierId);
            return supplier;
        }
    }
}
