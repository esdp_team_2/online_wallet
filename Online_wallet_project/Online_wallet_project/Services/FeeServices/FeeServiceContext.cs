﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;
using Online_wallet_project.Services.FeeServices;

namespace Online_wallet_project.Services.FeeServices
{
    public class FeeServiceContext
    {
        protected internal IFeeService FeeReportServiceStrategy;
        private readonly CommonFeeReportService _commonFeeReport;

        public FeeServiceContext(
            CommonFeeReportService commonFeeReport)
        {
            _commonFeeReport = commonFeeReport;
        }

        public void SetFeeReportStrategy(IFeeService feeServiceStrategy)
        {
            FeeReportServiceStrategy = feeServiceStrategy;
        }

        public async Task<SupplierModel> GetSupplier(int supplierId)
        {
            SupplierModel supplier = await _commonFeeReport.GetSupplier(supplierId);
            return supplier;
        }

        public SupplierFeeHistoryViewModel GetFeeHistoryModel(int supplierId)
        {
            SupplierFeeHistoryViewModel historyModel = FeeReportServiceStrategy.GetFeeHistoryModel(supplierId);
            return historyModel;
        }

        public async Task SaveReport(FeeReportSaveViewModel model)
        {
            await FeeReportServiceStrategy.SaveReport(model);
        }

        public async Task<decimal> GetFromAmountValue(int supplierId, DateTime dateOfStart, bool isEdited)
        {
            decimal fromAmount = await FeeReportServiceStrategy.GetFromAmountValue(supplierId, dateOfStart, isEdited);
            return fromAmount;
        }

        public async Task<DateTime> GetDateOfStartRewritten(int supplierId, int reportId)
        {
            DateTime dateOfStart = await FeeReportServiceStrategy.GetDateOfStartRewritten(supplierId, reportId);
            return dateOfStart;
        }
    }
}
