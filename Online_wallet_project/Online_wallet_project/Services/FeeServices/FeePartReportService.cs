﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.SupplierFeeViewModels;

namespace Online_wallet_project.Services.FeeServices
{
    public class FeePartReportService : CommonFeeReportService, IFeeService
    {
        public FeePartReportService(ApplicationDbContext context) : base(context)
        {
        }

        public SupplierFeeHistoryViewModel GetFeeHistoryModel(int supplierId)
        {
            SupplierFeeHistoryViewModel historyModel = GetOnlyFeeStatusHistoryModel(supplierId).Result;

            IQueryable<FeePartReport> feePartReports = GetSupplierFeePartReport(supplierId);

            historyModel.FeePartHistory = feePartReports.ToList();

            return historyModel;
        }

        public async Task SaveReport(FeeReportSaveViewModel model)
        {
            FeePartReport report = new FeePartReport
            {
                CalculatingMethod = model.SelectedMethod,
                SharedValue = decimal.Parse(model.FeePartReportSaveModel.SharedValue),
                DateOfCreation = DateTime.Now,
                DateOfStart = model.FeePartReportSaveModel.DateOfStart,
                SupplierId = model.FeePartReportSaveModel.SupplierId,
                CreatorId = model.Creator.Id,
            };
            await Context.FeePartReports.AddAsync(report);
            await Context.SaveChangesAsync();
        }

        public async Task<DateTime> GetDateOfStartRewritten(int supplierId, int reportId)
        {
            IQueryable<FeePartReport> feePartReports = GetSupplierFeePartReport(supplierId);
            FeePartReport report = await feePartReports.FirstOrDefaultAsync(r => r.Id == reportId);
            return report.DateOfStart;
        }

        public Task<decimal> GetFromAmountValue(int supplierId, DateTime dateOfStart, bool isEdited) => throw new NotImplementedException();

        public IQueryable<FeePartReport> GetSupplierFeePartReport(int supplierId)
        {
            IQueryable<FeePartReport> feePartReports =
                Context.FeePartReports.Where(r => r.SupplierId == supplierId)
                    .OrderBy(r => r.DateOfStart).ThenBy(r => r.DateOfCreation);

            return feePartReports;
        }
    }
}
