﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Online_wallet_project.Controllers;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Org.BouncyCastle.Asn1.Ocsp;

namespace Online_wallet_project.Services
{
    public class UserEmailChangeService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public UserEmailChangeService(
            ApplicationDbContext context, 
            UserManager<ApplicationUser> userManager,
            IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _emailSender = emailSender;
        }

        public async Task WriteInEmailChangeAsync(string userId, string currentEmail, string newEmail, bool isConfirmed)
        {
            UserEmailChange userEmailChange = new UserEmailChange
            {
                UserId = userId,
                CurrentEmail = currentEmail,
                NewEmail = newEmail,
                IsNewEmailConfirmed = isConfirmed,
                Date = DateTime.Now
            };

            await _context.UserEmailChanges.AddAsync(userEmailChange);
            await _context.SaveChangesAsync();
        }

        public async Task SendEmailChangeConfirmationAsync(ApplicationUser user, string newEmail, IUrlHelper url, HttpRequest request)
        {
            var code = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);
            var callbackUrl = url.ChangeEmailConfirmationLink(user.Id, code, newEmail, request.Scheme);
            var email = user.Email;
            string subject = "Подтвердите вашу смену текущей электронной почты";
            _emailSender.SendEmailChangeConfirmationAsync(email, subject, callbackUrl);
        }

        public async Task SendNewEmailConfirmationAsync(ApplicationUser user, IUrlHelper url, HttpRequest request)
        {
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = url.EmailConfirmationLink(user.Id, code, request.Scheme);
            var email = user.Email;
            _emailSender.SendEmailConfirmationAsync(email, callbackUrl);

            user.EmailConfirmed = false;
            await _userManager.UpdateAsync(user);
        }
    }
}
