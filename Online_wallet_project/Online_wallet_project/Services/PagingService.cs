﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Online_wallet_project.Services
{
    public class PagingService 
    {
        public PagedObject<T> DoPage<T>(IQueryable<T> items, int page) 
        {
            int pageSize = 10;
            var count = items.Count();
            var objects = items.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return new PagedObject<T>(count, pageSize, objects);
        }
    }

    public class PagedObject<T> 
    {
        private int count;
        private int pageSize;
        private List<T> objects;

        public int Count => count;

        public int PageSize => pageSize;

        public List<T> Objects => objects;

        public PagedObject(int count, int pageSize, List<T> objects)
        {
            this.count = count;
            this.objects = objects;
            this.pageSize = pageSize;
        }

    }
}
