﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Online_wallet_project.Services
{
    public class UserLockOutSettings
    {
        public bool? UserLockoutEnabledByDefault { get; set; }
        public double? DefaultLockoutTimeSpan { get; set; }
        public int? MaxFailedAccessAttempts { get; set; }

        public void UpdateJsonSettings()
        {
            System.IO.File.WriteAllText("userlockoutsettings.json",
                JsonConvert.SerializeObject(this, Formatting.Indented));
        }
    }
}
