﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Online_wallet_project.Data;
using Online_wallet_project.Models;
using Online_wallet_project.Models.ManageViewModels;

namespace Online_wallet_project.Services
{
    public class UserProfileChangeService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;
        private string _statusMessage;
        public UserProfileChangeService(
            UserManager<ApplicationUser> userManager,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _statusMessage = "Нету изменений";
        }

        public async Task<string> UpdateUserProfileViaEmail(ApplicationUser user, IndexViewModel model, IUrlHelper url, HttpRequest request)
        {
            bool isChanged = false;

            var phoneNumber = user.PhoneNumber;
            if (model.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, model.PhoneNumber);
                isChanged = true;
                if (!setPhoneResult.Succeeded)
                {
                    throw new ApplicationException($"Unexpected error occurred setting phone number for user with ID '{user.Id}'.");
                }
            }

            var firstName = user.FirstName;
            if (model.FirstName != firstName)
            {
                user.FirstName = model.FirstName;
                var setFirstNameResult = await _userManager.UpdateAsync(user);
                isChanged = true;
                if (!setFirstNameResult.Succeeded)
                {
                    throw new ApplicationException($"Unexpected error occurred setting first name for user with ID '{user.Id}'.");
                }
            }

            var lastName = user.LastName;
            if (model.Lastname != lastName)
            {
                user.LastName = model.Lastname;
                var setLastNameResult = await _userManager.UpdateAsync(user);
                isChanged = true;
                if (!setLastNameResult.Succeeded)
                {
                    throw new ApplicationException($"Unexpected error occurred setting last name for user with ID '{user.Id}'.");
                }
            }

            if (isChanged)
            {
                _statusMessage = "Твой профиль был обновлен";

                var callbackUrl = url.CheckUserProfileChangeLink(_statusMessage, request.Scheme);
                var email = user.Email;
                _emailSender.SendEmailUserProfileChangeAsync(email, _statusMessage, callbackUrl);
            }

            return _statusMessage;
        }
    }
}
