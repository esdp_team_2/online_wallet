﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Online_wallet_project.Data;
using Online_wallet_project.Models;

namespace Online_wallet_project.Services
{
    public class UserPasswordChangeService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public UserPasswordChangeService(
            ApplicationDbContext context, 
            UserManager<ApplicationUser> userManager, 
            IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _emailSender = emailSender;
        }

        public async Task WriteInPasswordChangeAsync(string userId, string oldPassword, string newPassword, string reason)
        {
            UserPasswordChange userPasswordChange = new UserPasswordChange
            {
                UserId = userId,
                OldPassword = oldPassword,
                NewPassword = newPassword,
                Reason = reason,
                Date = DateTime.Now
            };

            await _context.UserPasswordChanges.AddAsync(userPasswordChange);
            await _context.SaveChangesAsync();
        }

        public async Task SendChangePasswordLinkAsync(ApplicationUser user, IUrlHelper url, HttpRequest request)
        {
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = url.ChangePasswordCallbackLink(user.Id, code, request.Scheme);
            var email = user.Email;
            const string subject = "Смена пароля";
            _emailSender.SendEmailToSetPassword(email, subject, callbackUrl);
        }

        public async Task SendForgotPasswordLinkAsync(ApplicationUser user, string newEmail, IUrlHelper url, HttpRequest request)
        {
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = url.ResetPasswordCallbackLink(user.Id, code, request.Scheme);
            var email = user.Email;
            const string subject = "Сброс пароля";
            _emailSender.SendEmailToSetPassword(email, subject, callbackUrl);
        }
    }
}
