﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Data;
using Online_wallet_project.Models;

namespace Online_wallet_project.Services
{
    public class BlockedEverUsersService
    {
        private readonly ApplicationDbContext _context;

        public BlockedEverUsersService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void WriteUserInBlockedEverUsers(ApplicationUser user)
        {
            BlockedEverUser blockedUser = new BlockedEverUser
            {
                UserId = user.Id,
                DateOfStatusChange = DateTime.Now,
                StatusOfBlock = 1,
                Reason = "Превышение попыток неверного входа",
            };

            _context.BlockedEverUsers.AddAsync(blockedUser);
            _context.SaveChangesAsync();
        }

        public void UnWriteUserInBlockedEverUsers(ApplicationUser user)
        {           
            BlockedEverUser blockedUser = new BlockedEverUser
            {
                UserId = user.Id,
                DateOfStatusChange = DateTime.Now,
                StatusOfBlock = 2,
                Reason = "Разблокирован системой по истечению времени блокировки",
            };

            _context.BlockedEverUsers.AddAsync(blockedUser);
            _context.SaveChangesAsync();
        }
    }
}
