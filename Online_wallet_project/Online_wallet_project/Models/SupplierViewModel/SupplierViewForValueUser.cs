﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierViewForValueUser
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}
