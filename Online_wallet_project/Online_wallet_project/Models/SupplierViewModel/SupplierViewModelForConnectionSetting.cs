﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierViewModelForConnectionSetting
    {
        public int SupplierId { get; set; }
        public string DllPath { get; set; }
        public Dictionary<string, string> ConnectionValue { get; set; }
    }
}
