﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierRegisterViewModel
    {
        [Required (ErrorMessage = "Введите название поставщика")]
        [Display(Name = "Название поставщика")]
        public string SupplierName { get; set; }

        [Required (ErrorMessage = "Введите лицевой счет")]
        [Display(Name = "Лицевой счет")]
        public string PersonalAccount { get; set; }

        [Display(Name = "Код филиала")]
        public string BranchCode { get; set; }

        public IFormFile Logo { get; set; }

        [Display(Name = "Выберите группу")]
        public int GroupModelId { get; set; }
        public GroupModel GroupModel { get; set; }

        [Required (ErrorMessage = "Введите сумму ограничения")]
        [Display(Name = "Максимальная сумма оплаты")]
        public decimal AmountOfRestriction { get; set; }

        public string[] FieldName { get; set; }

        public string[] FieldType { get; set; }

        public int[] FieldLenghtData { get; set; }

        [Display(Name = "Неограниченна")]
        public bool IsRestrictionUpToInfinity { get; set; }
    }
}
