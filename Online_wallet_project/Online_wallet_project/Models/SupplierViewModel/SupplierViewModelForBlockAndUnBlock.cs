﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierViewModelForBlockAndUnBlock
    {
        public int SupplierID { get; set; }
        public string ManagerID { get; set; }

        [Display(Name = "Причина блокировки")]
        public string Description { get; set; }
    }
}
