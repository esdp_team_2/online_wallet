﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierFieldValueViewModel
    {
        public int SupplierIdSecond { get; set; }
        public string[] FieldNames { get; set; }
        public string[] FieldValues { get; set; }
    }
}
