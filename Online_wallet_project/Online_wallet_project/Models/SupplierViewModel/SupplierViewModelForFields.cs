﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierViewModelForFields
    {
        public SupplierModel Supplier { get; set; }
        public FieldTemplate[] FieldTemplates { get; set; }
        
        public string Message { get; set; }
    }
}
