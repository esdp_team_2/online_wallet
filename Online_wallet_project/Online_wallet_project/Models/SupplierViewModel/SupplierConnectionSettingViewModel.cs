﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierConnectionSettingViewModel
    {
        public int SupplierId { get; set; }
        public IFormFile DllSup { get; set; }
        public string[] FieldType { get; set; }
        public string[] FieldName { get; set; }
        public string ErrorMassage { get; set; }
    }
}
