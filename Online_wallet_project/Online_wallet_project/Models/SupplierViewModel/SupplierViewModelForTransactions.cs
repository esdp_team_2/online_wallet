﻿using Online_wallet_project.Models.JsonTransactionModel;
using System.ComponentModel.DataAnnotations;

namespace Online_wallet_project.Models.SupplierViewModel
{
    public class SupplierViewModelForTransactions
    {
        public int SupplierId { get; set; }
        public SupplierModel Supplier { get; set; }

        public string Json { get; set; }

        public decimal FeeSum { get; set; }

        [Required(ErrorMessage = "Это поле обязательное")]
        public decimal SumPay { get; set; }

        public decimal TotalSum { get; set; }

        public decimal UserBalance { get; set; }

        public CheckUserModel CheckUserModel { get; set; }

        public string Message { get; set; }

        public string JsonReportRequisite { get; set; }
    }
}
