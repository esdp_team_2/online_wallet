﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class FieldTemplate
    {
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public int FieldLenghtData { get; set; }
    }
}
