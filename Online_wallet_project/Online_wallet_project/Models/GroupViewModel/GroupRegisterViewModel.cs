﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Online_wallet_project.Models.GroupViewModel
{
    public class GroupRegisterViewModel
    {
        [Required(ErrorMessage = "Укажите имя группы")]
        [Display(Name = "Имя группы")]
        public string GroupName { get; set; }

        public IFormFile GroupLogo { get; set; }
    }
}
