﻿using System.Collections.Generic;
using Online_wallet_project.Models.ViewModels;

namespace Online_wallet_project.Models.SupplierUserViewModel
{
    public class SupplierPagingViewModel
    {
        public IEnumerable<Transaction> Transactions { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public int? SupplierId { get; set; }
    }
}
