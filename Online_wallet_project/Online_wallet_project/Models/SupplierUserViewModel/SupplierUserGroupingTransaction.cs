﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierUserViewModel
{
    public class SupplierUserGroupingTransaction
    {
        public decimal AllSum { get; set; }
        public decimal AllFee { get; set; }
        public DateTime Date { get; set; }
        public decimal? Profit { get; set; }
    }
}
