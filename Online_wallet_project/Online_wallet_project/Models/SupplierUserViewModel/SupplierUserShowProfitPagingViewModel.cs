﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Models.ViewModels;

namespace Online_wallet_project.Models.SupplierUserViewModel
{
    public class SupplierUserShowProfitPagingViewModel
    {
        public FilterViewModel FilterViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public List<SupplierUserGroupingTransaction> SupplierUserGroupingTransactions { get; set; }
        public int? SupplierId { get; set; }
    }
}
