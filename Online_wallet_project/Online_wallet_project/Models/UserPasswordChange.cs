﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class UserPasswordChange
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string Reason { get; set; }

        public DateTime Date { get; set; }
    }
}
