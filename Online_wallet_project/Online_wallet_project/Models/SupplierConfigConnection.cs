﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class SupplierConfigConnection
    {
        public int Id { get; set; }

        public int SupplierId { get; set; }
        public SupplierModel Supplier { get; set; }

        public string FieldData { get; set; }

        public string DllPath { get; set; }

        public string ConnectionValue { get; set; }
    }
}
