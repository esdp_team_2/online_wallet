﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public static class SupplierFeeStatusModel
    {
        public static List<string> FeeStatusList { get; } = new List<string> { "Без комиссии", "С комиссией"};

        public static string NoCommission { get; } = FeeStatusList.Find(s => s.Contains("Без комиссии"));

        public static string YesCommission { get; } = FeeStatusList.Find(s => s.Contains("С комиссией"));
    }

    public static class CalculatingMethodModel
    {
        public static List<string> CalcMethodList { get; } = new List<string> { "Процент", "Фикс. сом" };

        public static string Percent { get; } = CalcMethodList.Find(s => s.Contains("Процент"));

        public static string FixSom { get; } = CalcMethodList.Find(s => s.Contains("Фикс. сом"));
    }

    public class SupplierModel
    {
        public int Id { get; set; }
        public string SupplierName { get; set; }
        public string PersonalAccount { get; set; }
        public string BranchCode { get; set; }
        public string Logo { get; set; }
        public bool Blocking { get; set; }
        public int GroupModelId { get; set; }
        public GroupModel GroupModel { get; set; }
        public string FieldData { get; set; }
        public string ReportType { get; set; }
        public string Transport { get; set; }
        public decimal AmountOfRestriction { get; set; }
        public string TransactionIdSupplier { get; set; }
    }
}
