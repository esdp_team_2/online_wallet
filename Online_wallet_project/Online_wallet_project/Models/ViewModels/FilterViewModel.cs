﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.ViewModels
{
    public class FilterViewModel
    {
        public DateTime? SelectedDateFrom { get; private set; }
        public DateTime? SelectedDateTo { get; private set; }
        public decimal? SumFrom { get; private set; }
        public decimal? SumTo { get; private set; }
        public string SelectedName { get; private set; }


        public FilterViewModel(
            string name,
            DateTime? dateTimeFrom, DateTime? dateTimeTo,
            decimal? sumFrom, decimal? sumTo)
        {

            SelectedName = name;
            SelectedDateFrom = dateTimeFrom;
            SelectedDateTo = dateTimeTo;
            SumFrom = sumFrom;
            SumTo = sumTo;
        }
    }
}
