﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class SupplierFeeStatusChange
    {
        public int Id { get; set; }
        public int SupplierId { get; set; }
        public SupplierModel SupplierModel { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime DateOfStart { get; set; }
        public string FeeStatus { get; set; }
        public string CreatorId { get; set; }
        public ApplicationUser Creator { get; set; }
    }
}
