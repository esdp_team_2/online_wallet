﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class ShedullerSetting
    {
        public int Id { get; set; }
        public int StartTime { get; set; }
        public int PeriodTime { get; set; }
    }
}
