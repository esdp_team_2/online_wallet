﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class BlockedEverUser
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime DateOfStatusChange { get; set; }
        public string Reason { get; set; }
        public int StatusOfBlock { get; set; }
    }
}
