﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Models.ViewModels;

namespace Online_wallet_project.Models.ManageViewModels
{
    public class ManagePagingViewModel
    {
        public IEnumerable<Transaction> Transactions { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
