﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.ManageViewModels
{
    public class IndexViewModel
    {
        [Display(Name = "Логин")]
        public string Username { get; set; }

        [Display(Name = "Фамилия")]
        public string Lastname { get; set; }

        [Display(Name = "Имя пользователя")]
        public string FirstName { get; set; }

        [Display(Name = "Баланс на счету")]
        public Decimal Balance { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [Display(Name = "Почта")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public string StatusMessage { get; set; }

        public IEnumerable<Transaction> LastTransactions { get; set; }
    }
}
