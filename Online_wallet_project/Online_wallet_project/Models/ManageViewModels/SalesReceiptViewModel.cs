﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.ManageViewModels
{
    public class SalesReceiptViewModel
    {
        public string CompanyInfo { get; set; }
        public string TransactionId { get; set; }
        public string UserName { get; set; }
        public string SupplierName { get; set; }
        public string PersonalAccount { get; set; }
        public string SumPayAndFee { get; set; }
        public string SumFee { get; set; }
        public string SumPay { get; set; }
        public string DateOfCreate { get; set; }
        public string StatusName { get; set; }
    }
}
