﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Online_wallet_project.Models.ManageViewModels
{
    public class ChangeEmailViewModel
    {
        [Required(ErrorMessage = "Не указана новая почта")]
        [EmailAddress(ErrorMessage = "Неправильный формат почты")]
        [Display(Name = "Новая почта")]
        public string NewEmail { get; set; }

        [HiddenInput]
        public bool IsCurrentEmailConfirmed { get; set; }

        public string StatusMessage { get; set; }
    }
}
