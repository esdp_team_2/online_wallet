﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class BlackPassword
    {
        public int Id { get; set; }
        public string UnsuitablePassword { get; set; }
    }
}
