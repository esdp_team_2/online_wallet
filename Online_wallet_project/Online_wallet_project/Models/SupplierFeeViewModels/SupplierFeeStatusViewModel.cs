﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class SupplierFeeStatusViewModel
    {
        [HiddenInput]
        public int SupplierId { get; set; }

        public string Message { get; set; }

        [HiddenInput]
        public bool IsEdited { get; set; }

        [HiddenInput]
        public string SelectedFeeStatus { get; set; }

        [Display(Name = "Выберите наличие комисии")]
        public SelectList FeeTypeSelectList { get; set; }

        [Required(ErrorMessage = "Не указана дата старта")]
        [DataType(DataType.Date, ErrorMessage = "Неправильный формат даты")]
        [Display(Name = "Дата старта")]
        public DateTime DateOfStart { get; set; }

        public SupplierFeeHistoryViewModel FeeHistoryModel { get; set; }

        public SupplierFeeStatusViewModel()
        {
            FeeTypeSelectList = new SelectList(SupplierFeeStatusModel.FeeStatusList);
        }
    }
}
