﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class DateOfStartSelectViewModel
    {
        [HiddenInput]
        public int SupplierId { get; set; }

        [HiddenInput]
        public bool IsEdited { get; set; }

        [Display(Name = "Дата старта")]
        public DateTime DateOfStart { get; set; }
        
        [HiddenInput]
        public string MethodToRedirect { get; set; }
    }
}
