﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class SupplierFeePartViewModel
    {
        [HiddenInput]
        public int SupplierId { get; set; }

        [HiddenInput]
        public int ReportId { get; set; }

        public string Message { get; set; }

        [HiddenInput]
        public bool IsEdited { get; set; }

        [Required(ErrorMessage = "Не указана доля оператора")]
        [Display(Name = "Доля оператора")]
        public string SharedValue { get; set; }

        [Display(Name = "Дата старта")]
        public DateTime DateOfStart { get; set; }

        public SupplierFeeHistoryViewModel FeeHistoryModel { get; set; }

        [Display(Name = "Выберите способ подсчета")]
        public SelectList CalcMethodList { get; set; }

        public SupplierFeePartViewModel()
        {
            CalcMethodList = new SelectList(CalculatingMethodModel.CalcMethodList);
        }
    }
}
