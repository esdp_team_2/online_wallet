﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class FeeReportSaveViewModel
    {
        public SupplierFeeStatusViewModel FeeStatusReportSaveModel { get; set; }
        public SupplierTurnoverViewModel TurnoverReportSaveModel { get; set; }
        public SupplierFeePartViewModel FeePartReportSaveModel { get; set; }
        public FeePaidByUserViewModel FeePaidByUserReportSaveModel { get; set; }
        public ApplicationUser Creator { get; set; }
        public string SelectedMethod { get; set; }
    }
}
