﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class FeePaidByUserViewModel
    {
        [HiddenInput]
        public int SupplierId { get; set; }

        [HiddenInput]
        public int ReportId { get; set; }

        public string Message { get; set; }

        [HiddenInput]
        public bool IsEdited { get; set; }

        [Display(Name = "Сумма от")]
        public decimal FromAmount { get; set; }

        [Required(ErrorMessage = "Не указана сумма до")]
        [DataType(DataType.Currency, ErrorMessage = "Укажите числовую величину")]
        [Display(Name = "Сумма до")]
        public decimal ToAmount { get; set; }

        [Display(Name = "До макс. суммы")]
        public bool IsUpToMaxSum { get; set; }

        [Display(Name = "Выберите способ подсчета")]
        public SelectList CalcMethodList { get; set; }

        [Required(ErrorMessage = "Не указана комиссия")]
        [Display(Name = "Комиссия")]
        public string FeeValue { get; set; }

        [Display(Name = "Дата старта")]
        public DateTime DateOfStart { get; set; }

        public SupplierFeeHistoryViewModel FeeHistoryModel { get; set; }

        public FeePaidByUserViewModel()
        {
            CalcMethodList = new SelectList(CalculatingMethodModel.CalcMethodList);
        }
    }
}
