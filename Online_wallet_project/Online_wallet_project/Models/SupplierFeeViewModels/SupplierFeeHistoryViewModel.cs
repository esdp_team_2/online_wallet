﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class SupplierFeeHistoryViewModel
    {
        public SupplierModel Supplier { get; set; }

        public List<SuppliersTurnoverReport> TurnOverHistory { get; set; }

        public List<FeePartReport> FeePartHistory { get; set; }

        public List<Fee> FeePaidByUserHistory { get; set; }

        [HiddenInput]
        public int CurrentFeeStatusId { get; set; }

        public string FeeStatusName { get; set; }

        public List<SupplierFeeStatusChange> FeeStatusChanges { get; set; }
    }
}
