﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Online_wallet_project.Models.SupplierFeeViewModels
{
    public class SupplierTurnoverViewModel
    {
        [HiddenInput]
        public int SupplierId { get; set; }

        [HiddenInput]
        public int ReportId { get; set; }

        public string Message { get; set; }

        [HiddenInput]
        public bool IsEdited { get; set; }

        [Display(Name = "Сумма от")]
        public decimal FromAmount { get; set; }

        [Required(ErrorMessage = "Не указана сумма до")]
        [Display(Name = "Сумма до")]
        public decimal ToAmount { get; set; }

        [Display(Name = "И выше")]
        public bool UpToInfinity { get; set; }

        [Required(ErrorMessage = "Не указана доля оператора")]
        [Display(Name = "Доля оператора")]
        public string SharedValue { get; set; }

        [Display(Name = "Дата старта")]
        public DateTime DateOfStart { get; set; }

        [Display(Name = "Выберите способ подсчета")]
        public SelectList CalcMethodList { get; set; }

        [HiddenInput]
        public SupplierFeeHistoryViewModel FeeHistoryModel { get; set; }

        public SupplierTurnoverViewModel()
        {
            CalcMethodList = new SelectList(CalculatingMethodModel.CalcMethodList);
        }
    }
}
