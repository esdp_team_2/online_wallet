﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class StatusModel
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}
