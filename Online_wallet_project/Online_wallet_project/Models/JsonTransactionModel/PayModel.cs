﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.JsonTransactionModel
{
    public class PayModel
    {
        public string TransactionId { get; set; }
        public string Status { get; set; }
    }
}
