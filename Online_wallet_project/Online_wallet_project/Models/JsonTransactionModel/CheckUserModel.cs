﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.JsonTransactionModel
{
    public class CheckUserModel
    {
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PersonalAccaunt { get; set; }
        public string Status { get; set; }
    }
}
