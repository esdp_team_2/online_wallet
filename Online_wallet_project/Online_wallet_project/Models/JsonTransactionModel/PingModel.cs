﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.JsonTransactionModel
{
    public class PingModel
    {
        public string Status { get; set; }
    }
}
