﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Online_wallet_project.Models
{
    public class UserEmailChange
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public string CurrentEmail { get; set; }

        public string NewEmail { get; set; }

        public bool IsNewEmailConfirmed { get; set; }

        public DateTime Date { get; set; }
    }
}
