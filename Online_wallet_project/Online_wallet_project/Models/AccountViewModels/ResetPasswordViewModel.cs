﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Online_wallet_project.Models.AccountViewModels
{
    public class ResetPasswordViewModel
    {
        [HiddenInput]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан новый пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение нового пароля")]
        [Compare("Password", ErrorMessage = "Новый пароль не подтвердился")]
        public string ConfirmPassword { get; set; }

        [HiddenInput]
        public string Code { get; set; }
    }
}
