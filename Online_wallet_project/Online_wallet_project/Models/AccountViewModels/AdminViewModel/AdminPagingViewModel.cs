﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Models.ViewModels;

namespace Online_wallet_project.Models.AdminViewModel
{
    public class AdminPagingViewModel
    {
        public IEnumerable<ApplicationUser> Users { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public SortViewModel SortViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
