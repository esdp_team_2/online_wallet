﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Models.ViewModels;

namespace Online_wallet_project.Models.AdminViewModel
{
    public class ValidationSettingViewModel
    {
        public IEnumerable<BlackPassword> BlackPasswords { get; set; }
        public ValidationSettings ValidationSettings { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
