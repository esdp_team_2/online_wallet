﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_wallet_project.Services;

namespace Online_wallet_project.Models.AdminViewModel
{
    public class SettingsViewModel
    {
        public UserLockOutSettings UserLockOutSettings { get; set; }
        public SessionOptionSettings SessionOptionSettings { get; set; }
    }
}
