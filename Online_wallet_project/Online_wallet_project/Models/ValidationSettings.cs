﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class ValidationSettings
    {
        public int Id { get; set; }
        public int LengthPasswordInt { get; set; }
        public bool LengthPassword { get; set; }
        public bool PatternInt { get; set; }
        public bool PatternStringDown { get; set; }
        public bool PatternUPString { get; set; }
        public bool BlackPassword { get; set; }
        public bool PatternSpecialСharacters { get; set; }
    }
}
