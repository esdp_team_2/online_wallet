﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models.ManagerViewModels
{
    public class CreateSupplierUserVIewModel
    {
        [Required(ErrorMessage = "Это поле обязательное")]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Это поле обязательное")]
        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Подтвердите пароль")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли должы совпадать")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Выберите поставщика")]
        public int SupplierModelId { get; set; }
        public SupplierModel SupplierModel { get; set; }
    }
}
