﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Online_wallet_project.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public decimal MoneyBalance { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateOfCreation { get; set; }
        public bool IsLocked { get; set; }

        public int? SupplierId { get; set; }
    }
}
