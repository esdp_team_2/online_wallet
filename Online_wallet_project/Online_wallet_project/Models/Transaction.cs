﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int SupplierId { get; set; }
        public SupplierModel Supplier { get; set; } 
        public string TransactionСode { get; set; }
        public string Requisite { get; set; }
        public DateTime DateOfCreate { get; set; }
        public decimal SumPay { get; set; }
        public decimal SumFee { get; set; }
        public int StatusId { get; set; }
        public StatusModel Status { get; set; }
        public string StatusBody { get; set; }
        public bool IsFavorite { get; set; }
        public string GeneratedId { get; set; }
        public string ReportRequisite { get; set; }
    }
}
