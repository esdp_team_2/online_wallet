﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class BlockedSupplier
    {
        public int Id { get; set; }
        public int SupplierId { get; set; }
        public SupplierModel Supplier { get; set; }
        public string ManagerId { get; set; }
        public ApplicationUser Manager { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
