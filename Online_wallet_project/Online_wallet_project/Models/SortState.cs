﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public enum SortState
    {
        NameAsc,
        NameDesc,
        DateAsc,
        DateDesc
    }
}
