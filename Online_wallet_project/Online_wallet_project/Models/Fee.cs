﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_wallet_project.Models
{
    public class Fee
    {
        public int Id { get; set; }
        public DateTime DateOfStart { get; set; }
        public decimal SumMin { get; set; }
        public decimal SumMax { get; set; }
        public string CalculatingMethod { get; set; }
        public decimal FeeValue { get; set; }
        public DateTime DateOfCreation { get; set; }
        public string CreatorId { get; set; }
        public ApplicationUser Creator  { get; set; }
        public int SupplierId { get; set; }
        public SupplierModel SupplierModel { get; set; }
        public bool IsActive { get; set; }
        public bool Closed { get; set; }
    }
}
