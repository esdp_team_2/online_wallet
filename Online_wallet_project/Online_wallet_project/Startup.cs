﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Online_wallet_project.Data;
using Online_wallet_project.Extensions;
using Online_wallet_project.Models;
using Online_wallet_project.Services;
using Online_wallet_project.Services.FeeServices;
using System;
using System.IO;
using Microsoft.AspNetCore;
using Online_wallet_project.Extensions;
using Org.BouncyCastle.Asn1.X509.Qualified;

namespace Online_wallet_project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                    {
                        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes
                            (Configuration.GetValue<double>("DefaultLockoutTimeSpan"));
                        options.Lockout.MaxFailedAccessAttempts =
                            (Configuration.GetValue<int>("MaxFailedAccessAttempts"));
                        options.Lockout.AllowedForNewUsers =
                            (Configuration.GetValue<bool>("UserLockoutEnabledByDefault"));
                    }
            )
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes
                    (Configuration.GetValue<double>("IdleTimeout"));
            });

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<BlockedEverUsersService>();
            services.AddTransient<PagingService>();
            services.AddApplicationInsightsTelemetry(Configuration);
            services.Configure<UserLockOutSettings>(options => Configuration.Bind(options));
            services.Configure<SessionOptionSettings>(options => Configuration.Bind(options));
            services.AddTransient<FileUploadService>();
            services.AddIdentityCore<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 0;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredUniqueChars = 0;
            });
            services.AddTransient<UserEmailChangeService>();
            services.AddTransient<UserProfileChangeService>();
            services.AddTransient<UserPasswordChangeService>();
            services.AddTransient<TransactionExtention>();

            AddFeeReportsServices(services);

            services.AddMvc();
        }

        private static void AddFeeReportsServices(IServiceCollection services)
        {
            services.AddTransient<IFeeService, FeeStatusReportService>();
            services.AddTransient<IFeeService, TurnOverReportService>();
            services.AddTransient<IFeeService, FeePartReportService>();
            services.AddTransient<IFeeService, FeePaidByUserReportService>();

            services.AddTransient<FeeStatusReportService>();
            services.AddTransient<TurnOverReportService>();
            services.AddTransient<FeePartReportService>();
            services.AddTransient<FeePaidByUserReportService>();

            services.AddTransient<CommonFeeReportService>();
            services.AddTransient<FeeServiceContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile("userlockoutsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("sessionoptionssettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();    

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            Configuration = builder.Build();

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseSession();

            app.UseDeveloperExceptionPage();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
